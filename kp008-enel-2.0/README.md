# KP008-Enel-NLP Test Case Generation
### Manuale di utilizzo
Per ulteriori informazioni riguardanti i modelli usati visitare i seguenti link:

- [MarianMTModel](https://huggingface.co/docs/transformers/model_doc/marian)

- [JointBERT](https://github.com/monologg/JointBERT)

- [Graph2Tree](https://github.com/graph4ai/graph4nlp) da Graph4Nlp v.0.5.1

![This is the execution flow](./flow.png)

> ## 1. Analisi esecuzione - Translation component & Preparation component

```python
sh jb-analyze.sh "$file_name".txt
```

Esecuzione:

1. esegue la fase di traduzione del testo originale da lingua romanza a lingua inglese tramite il modello MarianMTModel.
   - INPUT: file “$file”.txt con elenco delle azioni
   - OUTPUT: tmp-dict.json (testo originale + testo tradotto) 

   Esempio:
```json
   {"WixFormTest": {"original": ["Apri il sitowww.francescogissi.wixsite.com/my-site/get-started"], "translation": ["Open www.francescogissi.wixsite.com/my-site/get-started"]}}
```
2. in cascata viene svolta la slot filling classification, l’intent classification tramite JointBERT  e la condition detection tramite Graph2Tree.
   - INPUT: tmp-dict.json (testo originale + testo tradotto)
   - OUTPUT: 
     - “$file”.analyzed.json
     - “$file”.condition.json
   Esempio:
```json  
   slot = [{"data": {"sequence": "www.francescogissi.wixsite.com/my-site/get-started"}, "id": "visiting"}]
   intent = {"0": "_invisible ( #sequence )"}
```
#### Comando extra:
```python
sh jb-prepare.sh "$file_name".txt
```
Viene eseguito solo quando esposto precedentemente al punto 2 riguardante la classificazione dei vari elementi.

> ## 2.Analisi esecuzione - BOOMSLANG component
```python
sh jb-vision.sh "$file_name" "$folder_name"
```
Tramite API di Selenium viene aperta una pagine del browser. Gli elementi identificati tramite JointBERT sono ricercati eseguendo OCR sullo screenshot del sito ed estrapola di ulteriori informazioni a riguardo.
- INPUT: 
  - “$file”.analyzed.json
  - “$file”.components.json
- OUTPUT: “$file”.resolved.json

*come parametro aggiuntivo si può decidere quale browser utilizzare. Di default è selezionato "chrome".

Esempio:
```json
{"action": "navigate", "x": -1, "y": -1, "xpath": "", "found": true, "to": "https:///www.enel.it"}
```
> ## 3.Analisi esecuzione - Build component
```python
sh jb-build.sh "$file_name" "$folder_name"
```
Estrapola dai json le condizioni identificate e i vari elementi interattivi identificati e, tramite un Parser, costruisce il test case in linguaggio Java.

- INPUT:
  - “$file”.condition.json
  - “$file”.resolved.json
- OUTPUT:“$file”.java (test case)

> ## 4.Analisi esecuzione - Run component
```python
sh jb-run.sh "$file_name"
```
Esegue il test case sfruttando Selenium per mostrare visibilmente l’esecuzione
- INPUT:“$file”.java (test case)
- OUTPUT: esito del test e tempi di esecuzione

### Comandi extra 
```python
sh gtt-init.sh "$file_name".java
```
Genera un file .txt cge contiene le azioni effettive eseguite per lo svolgimento del test case, concatenate con l’xpath dell’elemento con cui interagire.
```python
sh jb-run-test.sh "$file_name" "$folder_name"
```
Verifica se il testcase è stato generato e se presente lo lancia in esecuzione. Se invece il file java non è stato prodotto viene lanciato Boomslang e in seguito viene buildata la classe del test. Rispettivamente vengono quindi invocati nel secondo caso i comandi trattati al punto 2 e 3.
