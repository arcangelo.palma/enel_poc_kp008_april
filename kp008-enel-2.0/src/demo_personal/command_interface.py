import time
from io import BytesIO

# from tesserocr import PyTessBaseAPI, RIL, PSM
import easyocr
import numpy as np
from PIL import Image
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.relative_locator import locate_with
from tensorflow.keras.applications import mobilenet
from tensorflow.keras.layers import GlobalAveragePooling2D, Dense, Dropout
from tensorflow.keras.models import Model

from cc import mser_icons
from utils_command import *
from utils_command import _get_key, _get_xpath
# from pytesseract import Output
from yolov5.detect import detect


def build_model(base_model, n_classes):
    # base_model.trainable = False
    x = GlobalAveragePooling2D()(base_model.output)
    x = Dense(1024, activation="relu")(x)
    x = Dropout(0.25)(x)
    x = Dense(512, activation="relu")(x)
    x = Dropout(0.25)(x)
    y = Dense(n_classes, activation="softmax")(x)

    model = Model(inputs=base_model.input,
                  outputs=y)
    return model


class BaseCommand():
    '''
    Class for creating a base command. 

    Parameters:

    Returns:
    '''

    """constructor method"""

    def __init__(self, driver):
        self.driver = driver

    """execute method"""

    def execute(self):
        pass


class PathCommand(BaseCommand):

    def __init__(self, driver):
        super().__init__(driver)

    def execute(self, x_raw, y_raw):
        '''
        Returns the string that represents the XPATH starting from a coord point. 

        Parameters:
            x_raw (int): x coordinate 
            y_raw (int): y coordinate

        Returns:
            xpath(str):The string which gets XPATH.   
        '''
        return _get_xpath(self.driver, x_raw=x_raw, y_raw=y_raw)


class findElementsByCoordinates(PathCommand):
    def __init__(self, driver):
        super().__init__(driver)

    def execute(self, x_raw, y_raw, direction="below", offset=50):
        if direction == "below":
            y_raw = y_raw + offset
            return super().execute(x_raw, y_raw)
        elif direction == "above":
            y_raw = y_raw - offset
            return super().execute(x_raw, y_raw)
        elif direction == "right":
            x_raw = x_raw + offset
            return super().execute(x_raw, y_raw)
        elif direction == "left":
            x_raw = x_raw - offset
            return super().execute(x_raw, y_raw)

        print("Direction not recognized.")
        return None


class ScreenshotCommand(BaseCommand):

    def __init__(self, driver):
        super().__init__(driver)

    """execute method"""

    def execute(self, enabler=False,wait=False):
        '''
        Returns the reversed String.

        Parameters:
            enabler (bool): set enabler to True if you want to see the screenshot.

        Returns:
            data (numpy.array): Image as numpy array.    
        '''
        if wait:
            print("waiting...")
            time.sleep(10)
            print("stop waiting...")
        image = None
        image = self.driver.get_screenshot_as_png()
        img = Image.open(BytesIO(image))

        data = np.array(img)
        data = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)

        if enabler:
            gg = Image.fromarray(data)
            gg.show()

        return data



class RecTextEasyOcrCommand(BaseCommand):
    def __init__(self, driver):
        self.reader = easyocr.Reader(['en'])
        super().__init__(driver)

    '''Ritorna sia il top-left point che il bottom-right point'''

    def execute(self, image, word, instanceNumber=1, is_input_field=False):
        '''
            Command per il riconoscimento del testo.

        Parameters:
            image (numpy.array): Immagine come array
            word (str): la parola/stringa da cercare
            instanceNumber (optional, int): l'occorrenza di parola da cercare (la prima istanza, la seconda etc..)

        Returns:
            box (dict['x','y','text']): ritorna le coordinate riferite al primo punto (top-left) della prima lettera. 
        '''
        box = None
        detection = np.array(image)
        result = self.reader.readtext(detection)
        i1, i2 = 1, 1
        # print(i1,i2, instanceNumber)
        print("i'm searching string...")
        # print(result)
        for elem in result:

            startP = (int(elem[0][0][0]), int(elem[0][0][1]))
            endP = (int(elem[0][2][0]), int(elem[0][2][1]))
            #print(word, elem[1])
            '''
            if word == elem[1]:
                if i1 == instanceNumber:
                        box = {}
                        print("Found {} in bounding box {} with instance {}".format(word, elem[0], instanceNumber))
                        box['x'] = startP[0]
                        box['y'] = startP[1]
                        box['text'] = elem[1]
                        return box
                else:
                    print(word + " -> " + elem[1])
                    i1 = i1 + 1
            '''
            if diff_words(word.lower(), elem[1].lower()) is not None:
                if i2 == instanceNumber:
                        box = {}
                        print("Found {} in bounding box {} with instance {}".format(word, elem[0], instanceNumber))
                        box['x'] = startP[0]
                        box['y'] = startP[1]
                        box['text'] = elem[1]
                        return box

                else:
                    i2 = i2 + 1
            else:
                print(word + " -> " + elem[1])

        return box



'''Command per eseguire il click date le coordinate'''


class ClickCommand(BaseCommand):
    """constructor method"""

    def __init__(self, driver):
        super().__init__(driver)

    """execute method"""

    def execute(self, box):
        '''
        Esegue il click su una coordinata

        Parameters:
            box: coordinate su cui eseguire il click.

        Returns:
            None
        '''
        act = ActionChains(self.driver)
        act.reset_actions()
        act.move_by_offset(box['x'], box['y'])
        self.driver.implicitly_wait(3)
        act.click()
        act.perform()
        print("Clicked!")


'''Command per scrollare'''


class ScrollByCommand(BaseCommand):

    def __init__(self, driver):
        super().__init__(driver)

    def execute(self, offset, actual_pos):
        script = "window.scrollBy(0, " + str(offset) + ")"
        self.driver.execute_script(script)
        return offset


'''Command per  inizializzare, allargare la finestra e restituire uno screenshot della stessa'''


class InitWindowCommand(BaseCommand):

    def __init__(self, driver):
        super().__init__(driver)

    def execute(self, url):
        '''
        Apre il browser alla pagina specificata e ritorna l'immagine come numpy.array

        Parameters:
            url(str): URL da visitare. 

        Returns:
            image(numpy.array): screenshot come numpy array.
        '''
        self.driver.get(url)
        self.driver.maximize_window()
        self.driver.implicitly_wait(3)
        # acquisisci, converti e visualizza
        comm = ScreenshotCommand(self.driver)
        img = comm.execute(enabler=False)
        return img


def click_ex(driver, x_raw: int, y_raw: int):
    from const import XPATH_JS
    ev = f"""
        {XPATH_JS}
        var ev = new MouseEvent('click', {{
        'view': window,
        'bubbles': true,
        'cancelable': true,
        'screenX': {x_raw},
        'screenY': {y_raw}
        }});
        var el = document.elementFromPoint({x_raw}, {y_raw});
        el.dispatchEvent(ev);
        return getElementXPath(el); 
    """
    return driver.execute_script(ev)

def device_pixel_ratio(driver):
        return driver.execute_script("return window.devicePixelRatio")


def __scale_xy(driver, x, y):
    return int(x / device_pixel_ratio(driver)), int(y / device_pixel_ratio(driver))


def fill_text1(driver, x_raw, y_raw, text):
    x, y = __scale_xy(driver, x_raw, y_raw)
    return driver.execute_script(
            f'{XPATH_JS}\nel = document.elementFromPoint({x}, {y}); el.value="{text}"; return getElementXPath(el);'
        )


class ClickTextCommand():
    def __init__(self, driver, screenCommand, recognitionCommand, click):
        self.screen = screenCommand
        self.recognition = recognitionCommand
        self.driver = driver
        self.click = click

    def execute(self, word, instanceNumber=1, time=1):
        '''
        Clicca sul testo specificato

        Parameters:
            word(str): parola da cliccare
            instanceNumber(int): numero di instanza da ritornare 
)
        Returns:
            None
        '''
        self.driver.implicitly_wait(4)

        #freeze(driver=self.driver, seconds=time)
        data = self.screen.execute(enabler=False)

        print("i'm in click text...")
        box = self.recognition.execute(image=data, word=word, instanceNumber=instanceNumber)
        self.click.execute(box)
        #path = click_ex(self.driver, box["x"], box["y"])
        return box


class FieldNearTextCommand():
    def __init__(self, driver, screenCommand, recognitionCommand):
        self.screen = screenCommand
        self.recognition = recognitionCommand
        self.driver = driver

    def execute(self, word_for_search, word_to_write, input_type, position="near", instanceNumber=1, time=3):
        '''
        Cerca un field nelle vicinanze di un testo specificato.

        Parameters:
            word_to_search(str): parola da cercare.
            word_to_write(str): parola da inserire nel field.
            input_type(str): tipo di input se disponibile dall'HTML
            position(str): specificare l'orientamento (di default è "near"), altrimenti scegliere tra "below", "above", "right" e "left"

        Returns:
            None
        '''
        _pathcommand = PathCommand(self.driver)
        self.driver.implicitly_wait(3)
        import time as t2
        t2.sleep(5)
        data = self.screen.execute(enabler=False)
        box = self.recognition.execute(image=data, word=word_for_search, instanceNumber=instanceNumber)
        xpath = _pathcommand.execute(x_raw=box['x'], y_raw=box['y'])
        print("XPATH OF ELEMENT:", xpath)
        if position == "near" or position == "next":
            print("locate with: ", locate_with(By.TAG_NAME, input_type).near({By.XPATH: xpath}))
            locator = self.driver.find_element(locate_with(By.TAG_NAME, input_type).near({By.XPATH: xpath}))
            print("sono in near")
            print(locator.location)
            print("ID ELEMENT:", locator.get_property("id"))
            locator.send_keys(word_to_write)
            xp = "//*[@id=\""+locator.get_property("id")+ "\"]"
            return locator.location, locator.size, xp
        if position == "below" or position == "under":
            locator = self.driver.find_element(locate_with(By.TAG_NAME, input_type).below({By.XPATH: xpath}))
            print("sono in below")
            print(locator.location)
            locator.send_keys(word_to_write)
            return locator.location, locator.size
        if position == "above" or position == "up":
            locator = self.driver.find_element(locate_with(By.TAG_NAME, input_type).above({By.XPATH: xpath}))
            print("sono in above")
            print(locator.location)
            locator.send_keys(word_to_write)
            return locator.location, locator.size
        if position == "right":
            locator = self.driver.find_element(locate_with(By.TAG_NAME, input_type).to_right_of({By.XPATH: xpath}))
            print("sono in right")
            print(locator.location)
            locator.send_keys(word_to_write)
            return locator.location, locator.size
        if position == "left":
            locator = self.driver.find_element(locate_with(By.TAG_NAME, input_type).to_left_of({By.XPATH: xpath}))
            print("sono in left")
            print(locator.location)
            locator.send_keys(word_to_write)
            return locator.location, locator.size


class FillFieldTextCommand():
    def __init__(self, driver, screenCommand, recognitionCommand, pathcommand):
        self.screen = screenCommand
        self.recognition = recognitionCommand
        self.driver = driver
        self.pathcommand = pathcommand

    def execute(self, word, text_field, time, enter=False, instanceNumber=1, iif = False):
        '''
        Inserisce testo all'interno di un field esplicitato da un testo. 

        Parameters:
            word(str): Parola all'interno del field
            text_field(str): stringa da inserire all'interno del field
            enter(bool): settare a True se dopo l'inserimento si vuole premere "INVIO".
        Returns:
            None
        '''
        #self.driver.implicitly_wait(2)
        data = self.screen.execute(enabler=False)
        box = self.recognition.execute(image=data, word=word, instanceNumber=instanceNumber, is_input_field = iif)
        path = self.pathcommand.execute(x_raw=box['x'], y_raw=box['y'])
        initbutton = self.driver.find_element(By.XPATH, path)
        initbutton.send_keys(text_field)



        if enter:
            initbutton.send_keys(Keys.ENTER)
        return box, path





class classifyIconCommand(BaseCommand):
    def __init__(self, driver):
        super().__init__(driver)
        width, height = 224, 224
        target_size = (height, width)
        num_channels = 3
        input_shapes = (height, width, num_channels)
        base_model = mobilenet.MobileNet(include_top=False,
                                         weights='imagenet',
                                         input_shape=input_shapes)
        self.model = build_model(base_model, 49)
        weights = "../weights/weights_colab_mobilenet.hdf5"
        self.model.load_weights(weights)

    def execute(self, image):
        '''
        Classifica l'immagine specificata in input.

        Parameters:
            image(numpy.array): Immagine da classificare

        Returns:
            prediction(int): classe a cui appartiene l'immagine
        '''
        img = cv2.resize(image, (224, 224))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        stacked_img = cv2.Canny(img, 50, 100)
        stacked_img = np.stack((stacked_img,) * 3, axis=-1)
        imgg = np.expand_dims(stacked_img, axis=0)
        classTensor = self.model.predict(imgg)
        prediction = int(np.argmax(classTensor))
        #print("Predicted icon is a:", prediction)
        #print("---------")
        return prediction


class detectAndClassifyYoloCommand(BaseCommand):
    def __init__(self, driver):
        super().__init__(driver)

    def execute(self, img, className, viewer=False, class_view=False, mser=False):
        '''
        Esegue la detection e classification su uno screenshot specificato come input e ritorna il punto centrale in cui è stato trovato eventualmente la classe cercata. 

        Parameters:
            img(numpy.array): Screenshot di input
            className(str): classe da trovare
            class_view(bool): settare a True se si vuole vedere la classificazione sull'intero screenshot.

        Returns:
            None
        '''
        point = {}
        img2 = img.copy()
        if mser:
            # img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

            img = mser_icons(img)
        save_image(img)
        detections = detect(imgpath="../detections/", conf_thres=0.10, iou_thres=0.45, save_img=False, view_img=viewer)
        # print(detections.size())
        # print(detections)
        list_of_boxes = tensor2box(detections=detections)
        #print(list_of_boxes)
        clss = classifyIconCommand(self.driver)
        import pickle
        with open('../dictionary.pkl', 'rb') as f:
            loaded_dict = pickle.load(f)
        for el in list_of_boxes:
            icon = None

            icon = img2[el['y1']:el['y2'], el['x1']:el['x2']]

            obj_name = _get_key(clss.execute(icon), loaded_dict)
            #print("Found a: ", obj_name + " at coords: ", el)
            if class_view:
                cv2.rectangle(img2, (el['x1'], el['y1']), (el['x2'], el['y2']), (255, 0, 255), thickness=2)
                cv2.putText(img2, obj_name, (el['x1'], el['y1']), 0, 1, [225, 0, 255], thickness=2, lineType=cv2.LINE_AA)
            if obj_name == className:
                cop = img.copy()
                point = {'x': (el['x1'] + el['x2']) // 2, 'y': (el['y1'] + el['y2']) // 2}
                return point

        if class_view:
            from PIL import Image
            img = Image.fromarray(img2, 'RGB')
            img.show()

        return point


class searchUntilFoundTextCommand(BaseCommand):
    def __init__(self, driver, recognition, scroll, recoText):
        self.recognition = recognition
        self.scroll = scroll
        self.recoText = recoText
        super().__init__(driver)

    def execute(self, word, actual_pos, instanceNumber=1):
        found = False
        count = 0
        sample = 400
        while True:
            image = self.recognition.execute(enabler=False)
            # Image.fromarray(image).show()
            height, width, _ = image.shape
            box = self.recoText.execute(image=image, word=word, instanceNumber=instanceNumber)
            if box is not None:
                found = True
                return box, found, actual_pos

            if reached_end(self.driver):
                if count < 3:
                    actual_pos = self.scroll.execute(sample, actual_pos)
                    count += 1
                else:
                    target = self.driver.find_elements_by_xpath("//*[contains(text(), " + word + ")]")[0]
                    # target = self.driver.find_element(By.LINK_TEXT, word)
                    actions = ActionChains(self.driver)
                    actions.move_to_element(target).perform()
                    return None, found, actual_pos
            else:
                #time.sleep(1)
                actual_pos = self.scroll.execute(sample, actual_pos)

        return None, found, actual_pos


if __name__ == "__main__":
    gg = detect(imgpath="gg.jpg", conf_thres=0.15, iou_thres=0.45, save_img=False)
    # print(gg)
