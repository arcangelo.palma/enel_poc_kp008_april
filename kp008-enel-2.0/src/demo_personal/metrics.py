import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity


def create_dataframe(matrix, tokens):
    doc_names = [f'doc_{i + 1}' for i, _ in enumerate(matrix)]
    df = pd.DataFrame(data=matrix, index=doc_names, columns=tokens)
    return (df)


def cosine_sim(doc_1, doc_2):
    data = [doc_1.upper(), doc_2.upper()]
    count_vectorizer = CountVectorizer()
    vector_matrix = count_vectorizer.fit_transform(data)
    tokens = count_vectorizer.get_feature_names()
    vector_matrix.toarray()

    cosine_similarity_matrix = cosine_similarity(vector_matrix)
    return create_dataframe(cosine_similarity_matrix, ['doc_1', 'doc_2'])


def func(before, after):
    from skimage.metrics import structural_similarity
    import cv2
    import numpy as np

    # Convert images to grayscale
    before_gray = cv2.cvtColor(before, cv2.COLOR_BGR2GRAY)
    after_gray = cv2.cvtColor(after, cv2.COLOR_BGR2GRAY)

    # Compute SSIM between two images
    (score, diff) = structural_similarity(before_gray, after_gray, full=True)
    print("Image similarity", score)

    # The diff image contains the actual image differences between the two images
    # and is represented as a floating point data type in the range [0,1] 
    # so we must convert the array to 8-bit unsigned integers in the range
    # [0,255] before we can use it with OpenCV
    diff = (diff * 255).astype("uint8")

    # Threshold the difference image, followed by finding contours to
    # obtain the regions of the two input images that differ
    thresh = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]

    mask = np.zeros(before.shape, dtype='uint8')
    filled_after = after.copy()

    for c in contours:
        area = cv2.contourArea(c)
        if area > 40:
            x, y, w, h = cv2.boundingRect(c)
            cv2.rectangle(before, (x, y), (x + w, y + h), (36, 255, 12), 2)
            cv2.rectangle(after, (x, y), (x + w, y + h), (36, 255, 12), 2)
            cv2.drawContours(mask, [c], 0, (0, 255, 0), -1)
            cv2.drawContours(filled_after, [c], 0, (0, 255, 0), -1)

    cv2.imshow('before', before)
    cv2.imshow('after', after)
    cv2.imshow('diff', diff)
    cv2.imshow('mask', mask)
    cv2.imshow('filled after', filled_after)
    cv2.waitKey(0)


def charMap(word):
    word2 = word
    new_words = []
    mapping_dict = {'0': 'o', '1': 'i', 'ì': 'i', 'è': 'e', 'é': 'e', 'í': 'i', '/': 'i'}
    for elem in range(0, len(word)):
        for key in mapping_dict.keys():

            if (word2[elem] == key):
                word2 = word2[:elem] + mapping_dict[key] + word2[elem + 1:]

    return word2.upper()


if __name__ == "__main__":
    doc_1 = "Accedi"
    doc_2 = "Acc3di"
    new_word = charMap("Acced1")
    print(new_word)
