import random

import cv2

####DEFINE THE POSSIBLE COLORMAPS
colorMap = {1: cv2.COLORMAP_AUTUMN, 2: cv2.COLORMAP_BONE, 3: cv2.COLORMAP_JET, 4: cv2.COLORMAP_WINTER,
            5: cv2.COLORMAP_RAINBOW,
            6: cv2.COLORMAP_OCEAN, 7: cv2.COLORMAP_SUMMER, 8: cv2.COLORMAP_SPRING, 9: cv2.COLORMAP_COOL,
            10: cv2.COLORMAP_PINK, 11: cv2.COLORMAP_HOT,
            12: cv2.COLORMAP_PARULA, 13: cv2.COLORMAP_MAGMA, 14: cv2.COLORMAP_INFERNO, 15: cv2.COLORMAP_PLASMA,
            16: cv2.COLORMAP_VIRIDIS, 17: cv2.COLORMAP_CIVIDIS,
            18: cv2.COLORMAP_TWILIGHT_SHIFTED, 19: cv2.COLORMAP_TURBO}

im_gray = cv2.imread("test.jpg")
im_color = cv2.applyColorMap(im_gray, colorMap[random.choice(range(1, 20))])
if random.choice(range(0, 2)) == 1:
    im_color = cv2.flip(im_color, 1)
cv2.imshow("img", im_color)
cv2.waitKey(0)
im_color = cv2.applyColorMap(im_gray, colorMap[random.choice(range(1, 20))])
if random.choice(range(0, 2)) == 1:
    im_color = cv2.flip(im_color, 1)
cv2.imshow("img", im_color)
cv2.waitKey(0)

# FLIP HORIZONTAL
im_color = cv2.flip(im_color, 1)

# APPLY TO A GRAYSCALE IMAGE ANY COLOR MAP
