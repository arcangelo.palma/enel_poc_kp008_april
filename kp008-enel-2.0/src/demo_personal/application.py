import argparse
import json
import os
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

import command_interface
from command_interface import searchUntilFoundTextCommand, findElementsByCoordinates, \
    detectAndClassifyYoloCommand, FieldNearTextCommand, ClickTextCommand, FillFieldTextCommand, PathCommand, \
    ClickCommand, RecTextEasyOcrCommand, ScreenshotCommand, InitWindowCommand, ScrollByCommand, \
    ScrollByCommand
from utils_json import read_json





def iterate_json(list_of_actions, verbose):
    window = InitWindowCommand(driver=driver)
    screenshot = ScreenshotCommand(driver=driver)
    recognition = RecTextEasyOcrCommand(driver=driver)
    scroll = ScrollByCommand(driver)
    click = ClickCommand(driver)
    pathcommand = PathCommand(driver)
    text_field_inside = FillFieldTextCommand(driver, screenshot, recognition, pathcommand)
    text_field_near = FieldNearTextCommand(driver=driver, screenCommand=screenshot, recognitionCommand=recognition)
    clicktext = ClickTextCommand(driver, screenshot, recognition, click)

    coords = findElementsByCoordinates(driver=driver)
    detection = detectAndClassifyYoloCommand(driver=driver)
    search_until = searchUntilFoundTextCommand(driver, screenshot, scroll, recognition)
    list_of_new_actions = []


    flag = False
    flag_2 = False
    for action in list_of_actions:
        #print("i'm doing {}".format(action))
        command = {}
        actual_pos = 0
        if action["id"] == "visiting":
            img = window.execute(action["data"]["sequence"])

            command["action"] = "navigate"
            command["to"] = action["data"]["sequence"]
            command["found"] = True
            command["xpath"] = ""
            command["x"] = -1
            command["y"] = -1
            list_of_new_actions.append(command)

        elif action["id"] == "pressure" or action["id"] == "selection":
            if action["data"]["object"] == "button":
                time.sleep(2)
                instanceNumber = action["data"]["instanceNumber"] if "instanceNumber" in action["data"] else 1
                box = clicktext.execute(action["data"]["label"], instanceNumber=instanceNumber)

                command["action"] = "click_button"
                command["label_button"] = action["data"]["label"]
                command["x"] = box["x"]
                command["y"] = box["y"]
                p = PathCommand(driver)
                command["xpath"] = p.execute(command["x"]+1, command["y"]+1)
                #print(command["xpath"])

                command["is_modal"] = False
                command["found"] = True
                list_of_new_actions.append(command)

            elif action["data"]["object"] == "icon":
                time.sleep(2)
                #print("waiting for screen for icon detection..")
                if action["data"]["type"] == "user":

                    screen = screenshot.execute(enabler=False,wait=True)
                else:
                    screen = screenshot.execute(enabler=False, wait=False)

                if verbose == 1:
                    point = detection.execute(screen, className=action["data"]["type"],class_view=True, mser=False)
                else:
                    point = detection.execute(screen, className=action["data"]["type"], class_view=False, mser=False)
                #print(point)
                if action["data"]["type"] == "user" and point == {}:
                    loc = driver.find_element(By.XPATH, "//*[@id=\"globalHedaer\"]/div/div[3]/button[1]")
                    point = loc.location


                #print("click on ...", point)

                click.execute(point)


                command["action"] = "click_icon"
                command["icon_type"] = action["data"]["type"]
                command["x"] = point["x"]
                command["y"] = point["y"]
                p = PathCommand(driver)
                command["xpath"] = p.execute(command["x"], command["y"])
                command["is_modal"] = False
                command["found"] = True
                list_of_new_actions.append(command)

        elif action["id"] == "typing":
            if action["data"]["object"] == "input_field":
                if "position" in action["data"]:
                    time.sleep(2)
                     ## STO NEL CASO IN CUI DEVO TROVARE UN TEXTFIELD VICINO AD UNA LABEL
                    if "password" in action["data"]["label"]:
                        str1 = action["data"]["sequence"][:2]
                        #action["data"]["sequence"].replace(str1, str1.upper())

                    ist = action["data"]["instanceNumber"] if "instanceNumber" in action["data"] else 1
                    point, size, xpath1 = text_field_near.execute(word_for_search=action["data"]["label"],
                                                           word_to_write=action["data"]["sequence"], input_type="input",
                                                           position=action["data"]["position"],instanceNumber = ist)

                    command["action"] = "fill_input_label"
                    command["label"] = action["data"]["label"]
                    command["x"] = point["x"]+10
                    command["y"] = point["y"]+10
                    p = PathCommand(driver)
                    #print("I'm writing xpath for positional field...", p.execute(command["x"], command["y"]), "for the coords:", command["x"],command["y"])
                    command["xpath"] = xpath1 #p.execute(command["x"], command["y"])
                    command["txt"] = action["data"]["sequence"]
                    command["is_modal"] = False
                    command["found"] = True
                    list_of_new_actions.append(command)

                else:

                    time.sleep(2)
                    # img = screenshot.execute(enabler=False)
                    # im = Image.fromarray(img)
                    # im.show()
                    ist = action["data"]["instanceNumber"] if "instanceNumber" in action["data"] else 1
                    if "password" in action["data"]["label"]:
                        str1 = action["data"]["sequence"][:2]

                        action["data"]["sequence"].replace(str1, str1.upper())

                    if "cerc" in action["data"]["label"]:
                        #print(" ############ i'm in a search case!")
                        point,path = text_field_inside.execute(word=action["data"]["label"], text_field=action["data"]["sequence"],time=2,enter = True,
                                                 instanceNumber=ist, iif=True)
                    else:
                        point,path = text_field_inside.execute(word=action["data"]["label"], text_field=action["data"]["sequence"],
                                                 time=2, enter=False,
                                                 instanceNumber=ist, iif=True)


                    command["action"] = "fill_input_label"
                    command["label"] = action["data"]["label"]
                    command["x"] = point["x"] + 3
                    command["y"] = point["y"] + 3
                    p = PathCommand(driver)
                    command["xpath"] = path
                    command["txt"] = action["data"]["sequence"]
                    command["is_modal"] = False
                    command["found"] = True
                    list_of_new_actions.append(command)

        elif action["id"] == "scroll":
            if "length" in action["data"]:
                actual_pos = scroll.execute(action["data"]["length"], actual_pos=actual_pos)
                command["action"] = "fixed_scroll"
                command["length"] = action["data"]["length"]
                command["x"] = -1
                command["y"] = -1
                p = PathCommand(driver)
                list_of_new_actions.append(command)
            if "label" in action["data"]:


                box, found, actual_pos = search_until.execute(word=action["data"]["label"], actual_pos=actual_pos,
                                                              instanceNumber=1)


        elif (action["id"] == "ifany" or action["id"] == "ifnotany"):
            flag_2 = True
            if "sequence" in action["data"]:
                time.sleep(2)
                data = screenshot.execute(enabler=False)
                box = recognition.execute(image=data, word=action["data"]["sequence"])
                if box is not None:

                    command["action"] = "check_text"
                    p = PathCommand(driver)

                    command["x"] = box["x"] + 2
                    command["y"] = box["y"]
                    command["xpath"] = p.execute(command["x"], command["y"])

                    #print(command["xpath"])
                    if action["id"] == "ifnotany":
                        command["found"] = True
                    else:
                        command["found"] = True
                    list_of_new_actions.append(command)

                    # print(box)
                    print("Test OK.")
                    flag = True
                else:
                    print("Test not OK.")
                    flag = False

        else:
            print("Action not present.")

    if not flag_2 and not flag:
        flag = True
    return list_of_new_actions, flag


# def facebook(driver):
#     window = InitWindowCommand(driver=driver)
#     screenshot = ScreenshotCommand(driver=driver)
#     recognition = RecTextEasyOcrCommand(driver=driver)
#     scroll = ScrollByCommand(driver)
#     click = ClickCommand(driver)
#     pathcommand = PathCommand(driver)
#     filltext = FillFieldTextCommand(driver, screenshot, recognition, pathcommand)
#     search_and_write = FieldNearTextCommand(driver=driver, screenCommand=screenshot, recognitionCommand=recognition)
#     clicktext = ClickTextCommand(driver, screenshot, recognition, click)
#     recIcon = recIconCommand(driver)
#     detection = detectAndClassifyYoloCommand(driver=driver)
#     search_and_click = ClickCheckboxFieldNearTextCommand(driver=driver, screenCommand=screenshot,
#                                                          recognitionCommand=recognition)
#     data = window.execute(url="https://www.gamestop.it/")
#     clicktext.execute("ACCETTARE E PROSEGUIRE", time=1)
#     clicktext.execute("CHIUDERE", time=2)
#     time.sleep(2)
#     point = detection.execute(data, className="search", class_view=True, mser=True)
#
#
# def loginUseCaseFacebook(driver):
#     window = InitWindowCommand(driver=driver)
#     click = ClickCommand(driver=driver)
#     screenshot = ScreenshotCommand(driver=driver)
#     recognition = RecTextEasyOcrCommand(driver=driver)
#     scroll = ScrollByCommand(driver)
#     pathcommand = PathCommand(driver)
#     filltext = FillTextFieldCommand(driver, screenshot, recognition, click, pathcommand)
#
#     ###### USE CASE DELLA DEMO #####
#
#     data = window.execute(url="https://it-it.facebook.com/")  # raggiungi sito enel
#
#     ###STEP 1: ACCETTO I COOKIE
#     # command_interface.freeze(driver=driver, seconds=3.5)
#     data = screenshot.execute(enabler=False)
#     recognition.execute(data, word="Consenti cookie essenziali e facoltativi")
#     click.execute(box=box)
#
#     ###STEP 2: CERCO IL CAMPO EMAIL E INSERISCO EMAIL
#
#     ###STEP 3: CERCO IL CAMPO PASSWORD E INSERISCO
#
#     ###STEP 4: CERCO IL TASTO ACCEDI E PREMO
#
#     command_interface.freeze(driver=driver, seconds=3)
#     data = screenshot.execute(enabler=False)
#     box2 = recognition.execute(data, word='Accedi', log=True, alert=True)
#     # data = np.array(data)
#     # cv.rectangle(data,(box2['x'],box2['y']),(box2['x']+10,box2['y']+10),color=(0,255,255),thickness=2)
#
#     click.execute(box2)
#
#     command_interface.freeze(driver=driver, seconds=1)
#     data = screenshot.execute(enabler=False)
#     box = recognition.execute(data, word="Password")
#     initbutton = driver.find_element(By.XPATH, pathcommand.execute(x_raw=box['x'], y_raw=box['y']))
#     # initbutton = driver.find_element(By.ID, "passContainer")
#
#     initbutton.send_keys("acidrains")
#
#     command_interface.freeze(driver=driver, seconds=3)
#     data = screenshot.execute(enabler=False)
#     box2 = recognition.execute(data, word='Accedi', log=True, alert=True)
#
#
# def adidasCase(driver):
#     window = InitWindowCommand(driver=driver)
#     click = ClickCommand(driver=driver)
#     screenshot = ScreenshotCommand(driver=driver)
#     recognition = RecTextEasyOcrCommand(driver=driver)
#     scroll = ScrollByCommand(driver)
#     pathcommand = PathCommand(driver)
#     filltext = FillFieldTextCommand(driver, screenshot, recognition, pathcommand)
#     clicktext = ClickTextCommand(driver, screenshot, recognition, click)
#
#     firstScreen = window.execute("https://www.adidas.it/")
#
#     clicktext.execute("Accetto il monitoraggio", time=3)
#
#     filltext.execute(word="Cerca", text_field="zx", time=2)
#     clicktext.execute(word="Scarpe Forum Bold", time=2)
#
#
# def huaweiCase(driver):
#     window = InitWindowCommand(driver=driver)
#     click = ClickCommand(driver=driver)
#
#     screenshot = ScreenshotCommand(driver=driver)
#     recognition = RecTextEasyOcrCommand(driver=driver)
#     scroll = ScrollByCommand(driver)
#     pathcommand = PathCommand(driver)
#     filltext = FillFieldTextCommand(driver, screenshot, recognition, pathcommand)
#     clicktext = ClickTextCommand(driver, screenshot, recognition, click)
#
#     firstScreen = window.execute("https://consumer.huawei.com/it/")
#     clicktext.execute("ACCETTA I COOKIE", time=3)
#     scroll.execute(300)
#     clicktext.execute("Acquistaora", time=2)
#     clicktext.execute("Huawei P50 Pro", time=2)
#
#
# def samsungCas2e(driver):
#     window = InitWindowCommand(driver=driver)
#     click = ClickCommand(driver=driver)
#
#     screenshot = ScreenshotCommand(driver=driver)
#     recognition = RecTextEasyOcrCommand(driver=driver)
#     scroll = ScrollByCommand(driver)
#     pathcommand = PathCommand(driver)
#     filltext = FillFieldTextCommand(driver, screenshot, recognition, pathcommand)
#     clicktext = ClickTextCommand(driver, screenshot, recognition, click)
#
#     firstScreen = window.execute("https://www.amazon.it")
#     point = detection.execute()
#
#
# def searchOnZalandoCase(driver):
#     window = InitWindowCommand(driver=driver)
#     click = ClickCommand(driver=driver)
#     screenshot = ScreenshotCommand(driver=driver)
#     recognition = RecTextEasyOcrCommand(driver=driver)
#     scroll = ScrollByCommand(driver)
#     pathcommand = PathCommand(driver)
#     ###### USE CASE DELLA DEMO #####
#     data = window.execute(url="https://www.zalando.it/")  # raggiungi sito enel
#
#     ###STEP 1: ACCETTO I COOKIE
#     # command_interface.freeze(driver=driver, seconds=3.5)
#     data = screenshot.execute(enabler=False)
#     box = recognition.execute(data, word="Accetto")
#     click.execute(box=box)
#
#     ###STEP 3: Focus su ricerca
#     data = screenshot.execute(enabler=False)
#     box = recognition.execute(data, word="Ricerca")
#
#     initbutton = driver.find_element(By.XPATH, pathcommand.execute(x_raw=box['x'], y_raw=box['y']))
#     initbutton.send_keys("scarpe nike")
#
#     ###STEP 4: Clicca sul prodotto AIR FORCE 1
#     data = screenshot.execute(enabler=False)
#     box = recognition.execute(data, word="AIR FORCE 1")
#     click.execute(box=box)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--browser', choices=['chrome', 'firefox'], type=str, default='chrome', required=True)
    parser.add_argument("--json_file", type=str, required=True)
    parser.add_argument("--json", type=str, required=True)
    parser.add_argument("--verbose",type=str,required=True)
    args = parser.parse_args()

    if args.browser == "firefox":
        driver = webdriver.Firefox(executable_path='../drivers/geckodriver')

    if args.browser == "chrome":
        chr_options = Options()
        chr_options.add_experimental_option("detach", True)
        #chr_options.add_argument("--force-dark-mode")
        driver = webdriver.Chrome(executable_path='../drivers/chromedriver', options=chr_options)

    ex_file = "../" + args.json_file
    list_of_actions = read_json(ex_file)
    verb = int(args.verbose)
    list_json, flag = iterate_json(list_of_actions=list_of_actions, verbose = verb)

    with open("../" + args.json, 'w') as fout:
        json.dump(list_json, fout, indent=4)

    time.sleep(5)
    if not flag:
        os.remove("../{}".format(args.json))

    driver.close()
    driver.quit()
