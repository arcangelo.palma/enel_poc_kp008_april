import pickle
from difflib import get_close_matches

import cv2
from selenium.common.exceptions import JavascriptException

from const import XPATH_JS


def diff_words(str1, str2):
    cut = 0
    if len(str1) < 6 or len(str2) < 6:
        cut = 0.80
    else:
        cut = 0.85

    result = get_close_matches(str1, [str2], n=1, cutoff=cut)
    return result[0] if len(result) > 0 else None


def save_image(img):
    name = "imtemp.jpg"
    cv2.imwrite("../detections/imtemp.jpg", img)


"""Class Dedicated to Command"""


def reached_end(driver) -> bool:
    try:
        return driver.execute_script(
            "return (window.innerHeight + window.scrollY) >= document.body.offsetHeight"
        )
    except JavascriptException:
        return False


def detections_from_file(path):
    list_of_boxes = []
    with open(path, 'rb') as f:
        loaded_dict = pickle.load(f)

    list_of_det = loaded_dict[0]

    for el in list_of_det:
        box = {'class': int(el[5]), 'x1': int(el[0].cpu().detach().numpy()), 'y1': int(el[1].cpu().detach().numpy()),
               'x2': int(el[2].cpu().detach().numpy()), 'y2': int(el[3].cpu().detach().numpy())}
        list_of_boxes.append(box)
    img = cv2.imread("screen_for/prova.jpg")

    # cv2.rectangle(img, (box['x1'],box['y1']), (box['x2'],box['y2']), (255, 0, 255), 1)
    list_of = []
    with open('/home/kebula/Desktop/experiments/yolov5/runs/detect/exp2/labels/image.txt') as f:
        lines = f.readlines()

    for el in lines:
        list_of.append([int(s) for s in el.split() if s.isdigit()])

    '''
    for el in list_of:
        cv2.rectangle(img,(el[1],el[2]),(el[3],el[4]),(255,0,255),2)
    '''
    return img, list_of


def _sub_images():
    img, list_of = detections_from_file("detections/saved_detections.pkl")
    i = 0
    for el in list_of:
        name_icon = "icon" + str(i) + ".jpg"
        filepath = "detections/" + name_icon
        cropped = img[el[2]:el[4], el[1]:el[3]]
        cv2.imwrite(filepath, cropped)
        i += 1


def tensor2box(detections):
    list_of_boxes = []
    num_det = detections.size(0)

    for i in range(0, num_det):
        box = {}
        box['x1'] = int(detections[i][0])
        box['y1'] = int(detections[i][1])
        box['x2'] = int(detections[i][2])
        box['y2'] = int(detections[i][3])
        list_of_boxes.append(box)
    return list_of_boxes


def _get_key(val, my_dict):
    for key, value in my_dict.items():
        if val == value:
            return key

    return "key doesn't exist"


def freeze(driver, seconds=3):
    driver.implicitly_wait(seconds)


def _device_pixel_ratio(driver):
    return driver.execute_script("return window.devicePixelRatio")


def _scale_xy(driver, x: int, y: int):
    return int(x / _device_pixel_ratio(driver)), int(y / _device_pixel_ratio(driver))


def _get_xpath(driver, x_raw: int, y_raw: int) -> str:
    x, y = _scale_xy(driver, x_raw, y_raw)
    return driver.execute_script(
        f'{XPATH_JS}\n el = document.elementFromPoint({x}, {y}); return getElementXPath(el);'
    )


def __get_key(self, val, my_dict):
    for key, value in my_dict.items():
        if val == value:
            return key

    return "key doesn't exist"
