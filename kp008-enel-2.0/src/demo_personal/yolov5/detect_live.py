from pathlib import Path

import cv2
import torch
from numpy import random

from models.experimental import attempt_load
from utils.general import non_max_suppression, scale_coords, set_logging
from utils.plots import plot_one_box

# save_dir = Path(increment_path(Path("") / opt.name, exist_ok=opt.exist_ok))  # increment run
# (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True, exist_ok=True)  # make dir

# Initialize
set_logging()
device = torch.device('cpu')
weights = "best_yolo2.pt"
model = attempt_load(weights, map_location=device)
names = model.module.names if hasattr(model, 'module') else model.names
colors = [[random.randint(0, 255) for _ in range(3)] for _ in names]
print(model)
half = False
imgg = cv2.imread("/home/kebula/Desktop/experiments/yolov5_detection/yolov5/gg.png")
img = cv2.resize(imgg, (640, 640))
# img = cv2.resize()
img = torch.from_numpy(img)
img = img.half() if half else img.float()  # uint8 to fp16/32
img /= 255.0  # 0 - 255 to 0.0 - 1.0
if img.ndimension() == 3:
    img = img.unsqueeze(0)
print(img.size())
# Inference
img2 = img.permute(0, 3, 1, 2)
print(img2.size())
pred = model(img2)[0]
pred = non_max_suppression(pred, 0.25, 0.45)
webcam = False
save_txt = False
save_dir = "runs/detect"
save_img = False
save_conf = False
view_img = True
path = "/home/kebula/Desktop/experiments/yolov5_detection/yolov5"

# Process detections
for i, det in enumerate(pred):  # detections per image
    if webcam:  # batch_size >= 1
        p, s, im0, frame = path[i], '%g: ' % i, im0s[i].copy(), dataset.count
    else:
        p, s, im0 = path, '', imgg

    p = Path(p)  # to Path
    # save_path = str(save_dir / p.name)  # img.jpg
    # txt_path = str(save_dir / 'labels' / p.stem) + ('' if dataset.mode == 'image' else f'_{frame}')  # img.txt
    s += '%gx%g ' % img.shape[2:]  # print string
    gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
    if len(det):
        # Rescale boxes from img_size to im0 size
        det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

        # Print results
        for c in det[:, -1].unique():
            n = (det[:, -1] == c).sum()  # detections per class
            s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string
        print(det)
        # Write results
        for *xyxy, conf, cls in reversed(det):

            if save_img or view_img:  # Add bbox to image
                label = f'{names[int(cls)]} {conf:.2f}'
                plot_one_box(xyxy, im0, label=label, color=colors[int(cls)], line_thickness=2)

    # Print time (inference + NMS)
    # print(f'{s}Done. ({t2 - t1:.3f}s)')

    # Stream results
    if view_img:
        cv2.imshow("img", im0)
        cv2.waitKey(0)  # 1 millisecond

    # Save results (image with detections)
    if save_img:
        if dataset.mode == 'image':
            cv2.imwrite(save_path, im0)
        else:  # 'video'
            if vid_path != save_path:  # new video
                vid_path = save_path
                if isinstance(vid_writer, cv2.VideoWriter):
                    vid_writer.release()  # release previous video writer

                fourcc = 'mp4v'  # output video codec
                fps = vid_cap.get(cv2.CAP_PROP_FPS)
                w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                vid_writer = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*fourcc), fps, (w, h))
            vid_writer.write(im0)
