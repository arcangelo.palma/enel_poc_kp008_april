import time

import cv2
import numpy as np
from PIL import Image, ImageEnhance, ImageOps
from mss import mss
from pytorchYOLOv4.models_arged import YoloIstant
from tesserocr import PyTessBaseAPI, RIL, PSM


def word_in_image(image, word, reverse=False, augmentation=True, dup=True):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    if dup:
        wh = (int(image.shape[1] * 2), int(image.shape[0] * 2))
        image = cv2.resize(image, wh, interpolation=cv2.INTER_CUBIC)
    # applying grayscale method
    image = Image.fromarray(image)

    print("RISULTATI CON TESSEROCR ****************************************")
    with PyTessBaseAPI(**{'lang': "eng+ita"}, psm=PSM.SPARSE_TEXT_OSD) as api:
        print("WORD AND SENTENCES FOUND IN THE IMAGE: \n")
        api.SetImage(image)
        boxes = api.GetComponentImages(RIL.TEXTLINE, True)
        print('Found {} textline image components.'.format(len(boxes)))
        for i, (im, box, _, _) in enumerate(boxes):
            api.SetRectangle(box['x'], box['y'], box['w'], box['h'])
            ocrResult = api.GetUTF8Text()
            conf = api.MeanTextConf()

            print(ocrResult)
            if word in ocrResult:
                bb = {'x': box["x"] // 2, 'y': box["y"] // 2}
                return bb

    return None


if __name__ == "__main__":
    mon = {'left': 0, 'top': 120, 'width': 1900, 'height': 960}

    model = YoloIstant(n_classes=1, weightfile="/home/kebula/Downloads/best_yolov5_customdataset.pt",
                       namesfile="_classes.txt")

    with mss() as sct:
        while True:

            screenShot = sct.grab(mon)
            img = Image.frombytes(
                'RGB',
                (screenShot.width, screenShot.height),
                screenShot.rgb,
            )

            # decreasing the brightness 20%
            img = ImageEnhance.Brightness(img).enhance(0.9)

            # increasing the contrast 20%
            img = ImageEnhance.Contrast(img).enhance(1.5)
            img = ImageOps.autocontrast(img)
            # img = cv2.Canny(np.array(img), 50,100)
            # img = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2GRAY)
            # img = Image.fromarray(img)
            # detection = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)
            detection = Image.fromarray(np.array(img))
            # word_in_image(np.array(detection), word="Accetto")
            detection = model.detection(detection)

            detection = cv2.cvtColor(np.array(detection), cv2.COLOR_BGR2RGB)
            # detection.show()
            cv2.imshow('test', np.array(detection))
            time.sleep(2)
            if cv2.waitKey(33) & 0xFF in (
                    ord('q'),
                    27,
            ):
                break
