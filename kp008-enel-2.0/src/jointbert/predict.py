import argparse
import logging
import os
import re

import numpy as np
import torch
from torch.utils.data import TensorDataset, DataLoader, SequentialSampler
from tqdm import tqdm
from word2number import w2n

from src.jointbert.utils import init_logger, load_tokenizer, get_intent_labels, get_slot_labels, MODEL_CLASSES

logger = logging.getLogger(__name__)


def get_device(pred_config):
    return "cuda" if not pred_config.no_cuda and torch.cuda.is_available() else "cpu"


def get_args(pred_config):
    return torch.load(os.path.join(pred_config.model_dir, 'training_args.bin'))


def load_model(pred_config, args, device):
    # Check whether model exists
    if not os.path.exists(pred_config.model_dir):
        raise Exception("Model doesn't exists! Train first!")

    try:
        model = MODEL_CLASSES[args.model_type][1].from_pretrained(args.model_dir,
                                                                  args=args,
                                                                  intent_label_lst=get_intent_labels(args),
                                                                  slot_label_lst=get_slot_labels(args))
        model.to(device)
        model.eval()
    except FileNotFoundError:
        print(FileNotFoundError)
        raise Exception("Some model files might be missing...")

    return model


def get_nearest_seq(words, start):
    dx = 1
    sx = 1
    while start + dx < len(words) and not (words[start + dx][0] == "\"" and words[start + dx][-1] == "\""):
        dx += 1
    while start - sx >= 0 and not (words[start - sx][0] == "\"" and words[start - sx][-1] == "\""):
        sx += 1

    if dx == sx:
        return -sx
    elif dx < sx:
        return dx
    else:
        return -sx


def preprocess_input(in_lines):
    out_lines = []
    ob_type = ['button', 'input_field', 'label', 'text', 'string', 'tag']
    icon_type = ['menu', 'download', 'upload', 'user', 'close', 'add', 'cart', 'favourite', 'like', 'unlike',
                 'home', 'refresh', 'searchbar', 'search', "arrow", "arrow up", "arrow right", "arrow down",
                 "arrow left"]
    num = ['first', 'second', 'third', 'fourth', 'fifth']
    for line in in_lines:
        # preprocessing to labeling button and icon
        line = " " + line.strip() + " "
        line = re.sub(r" [d|D]rop[_ -]?down ", " dropdown ", line)
        line = re.sub(r" [s|S]earch[_ -]?bar ", " searchbar ", line)

        line = re.sub(r"'s |, ", " ", line)
        line = re.sub(r" (([s|S]earch|[e|E]ntry)[_ -])?(([i|I]nput|[t|T]ext)[_ -])?[f|F]ield ", " input_field ", line)
        for element in icon_type:
            element = element.replace(" ", "-")
            regex = "icon ['|\"]*" + element + "['|\"]*|['|\"]*" + element + "['|\"]* icon| " + element + " "
            line = re.sub(regex, element.replace("\"", "").replace("'", "") + "_icon", line, re.I)
        line = re.sub(r" [l|L]og[_ -]?[i|I]n ", " log-in_button ", line)
        line = re.sub(r" [l|L]og[_ -]?[o|O]ut ", " log-out_button ", line)
        line = re.sub(r" [r|R]adio[_ -]?[b|B]utton ", " radio_button ", line)
        line = re.sub(r" [c|C]heck[_ -][m|M]ark ", " check-mark_button ", line)
        line = re.sub(r" [c|C]heck[_ -]?[b|B]ox ", " check-box_button ", line)
        line = re.sub(r" (([i|I]nput|[t|T]ext)[_ -])?[b|B]ox ", " input_field ", line)
        line = re.sub(r" ([d|D]ownstairs)|([u|U]pstairs) ", " scroll ", line)
        line = re.sub(r" ([G|g]o) to ", " visiting ", line)
        line = re.sub(r" ([G|g]et) down ", " scroll ", line)
        line = line.replace("  ", " ")

        if line[-1] == ".":
            line = line[0:-1] + " "
        else:
            line += " "

        # preprocessing url
        matches = re.findall(r"([\"|']?(http:|https:|www\.)\S*[\"|']? )", line, re.MULTILINE)
        for match in matches:
            line = re.sub(match[0], match[0].replace("\"", ""), line)

        # preprocessing labeling sequence
        line = line.replace("'", "\"")
        line = re.sub(r"[()]", "", line)
        matches = re.findall(r"[a-zA-Z]\"[a-zA-Z]", line, re.MULTILINE)
        for match in matches:
            line = line.replace(match, match.replace("\"", "'"))

        matches = re.findall(r" \"[^\"]+\" ", line, re.MULTILINE)
        for match in matches:
            line = line.replace(match, " " + match[1:-1].replace(" ", "^") + " ")

        flag = False
        words = []
        for w in line.split()[::-1]:
            if w.lower() == "choice" or w.lower() == "option":
                flag = True
            else:
                try:
                    if flag:
                        words.append("option_" + str(w2n.word_to_num(w)))
                        flag = False
                    else:
                        words.append(str(w2n.word_to_num(w)))
                except ValueError:
                    if flag and w.lower() in num:
                        words.append("option_" + w)
                        flag = False
                    else:
                        words.append(w)

        words = words[::-1]
        for i in range(len(words)):
            if words[i] in ob_type:
                try:
                    pos = get_nearest_seq(words, i)
                    words[i + pos] = "label_" + words[i + pos]
                except IndexError:
                    continue
        out_lines.append(" ".join(words))
    return out_lines


def convert_input_file_to_tensor_dataset(lines,
                                         pred_config,
                                         args,
                                         tokenizer,
                                         pad_token_label_id,
                                         cls_token_segment_id=0,
                                         pad_token_segment_id=0,
                                         sequence_a_segment_id=0,
                                         mask_padding_with_zero=True):
    # Setting based on the current model type
    cls_token = tokenizer.cls_token
    sep_token = tokenizer.sep_token
    unk_token = tokenizer.unk_token
    pad_token_id = tokenizer.pad_token_id

    all_input_ids = []
    all_attention_mask = []
    all_token_type_ids = []
    all_slot_label_mask = []

    for words in lines:
        tokens = []
        slot_label_mask = []
        for word in words.split():
            word_tokens = tokenizer.tokenize(word)
            if not word_tokens:
                word_tokens = [unk_token]  # For handling the bad-encoded word
            tokens.extend(word_tokens)
            # Use the real label id for the first token of the word, and padding ids for the remaining tokens
            slot_label_mask.extend([pad_token_label_id + 1] + [pad_token_label_id] * (len(word_tokens) - 1))

        # Account for [CLS] and [SEP]
        special_tokens_count = 2
        if len(tokens) > args.max_seq_len - special_tokens_count:
            tokens = tokens[: (args.max_seq_len - special_tokens_count)]
            slot_label_mask = slot_label_mask[:(args.max_seq_len - special_tokens_count)]

        # Add [SEP] token
        tokens += [sep_token]
        token_type_ids = [sequence_a_segment_id] * len(tokens)
        slot_label_mask += [pad_token_label_id]

        # Add [CLS] token
        tokens = [cls_token] + tokens
        token_type_ids = [cls_token_segment_id] + token_type_ids
        slot_label_mask = [pad_token_label_id] + slot_label_mask

        input_ids = tokenizer.convert_tokens_to_ids(tokens)

        # The mask has 1 for real tokens and 0 for padding tokens. Only real tokens are attended to.
        attention_mask = [1 if mask_padding_with_zero else 0] * len(input_ids)

        # Zero-pad up to the sequence length.
        padding_length = args.max_seq_len - len(input_ids)
        input_ids = input_ids + ([pad_token_id] * padding_length)
        attention_mask = attention_mask + ([0 if mask_padding_with_zero else 1] * padding_length)
        token_type_ids = token_type_ids + ([pad_token_segment_id] * padding_length)
        slot_label_mask = slot_label_mask + ([pad_token_label_id] * padding_length)

        all_input_ids.append(input_ids)
        all_attention_mask.append(attention_mask)
        all_token_type_ids.append(token_type_ids)
        all_slot_label_mask.append(slot_label_mask)

    # Change to Tensor
    all_input_ids = torch.tensor(all_input_ids, dtype=torch.long)
    all_attention_mask = torch.tensor(all_attention_mask, dtype=torch.long)
    all_token_type_ids = torch.tensor(all_token_type_ids, dtype=torch.long)
    all_slot_label_mask = torch.tensor(all_slot_label_mask, dtype=torch.long)

    dataset = TensorDataset(all_input_ids, all_attention_mask, all_token_type_ids, all_slot_label_mask)
    return dataset


# return cleaned lines of input command of test
# return slot list when every element matching with a word in lines
# return intent detected element
def predict(root, lines, pred_config):
    # load model and args
    args = get_args(pred_config)
    args.data_dir = os.path.join(root, args.data_dir)
    # added
    args.model_dir = pred_config.model_dir

    device = get_device(pred_config)
    model = load_model(pred_config, args, device)

    intent_label_lst = get_intent_labels(args)
    slot_label_lst = get_slot_labels(args)

    # Convert input file to TensorDataset
    pad_token_label_id = args.ignore_index
    # load tokenizer of select pre-trained model
    tokenizer = load_tokenizer(args)
    lines = preprocess_input(lines)
    dataset = convert_input_file_to_tensor_dataset(lines, pred_config, args, tokenizer, pad_token_label_id)

    # Predict
    sampler = SequentialSampler(dataset)
    data_loader = DataLoader(dataset, sampler=sampler, batch_size=pred_config.batch_size)

    all_slot_label_mask = None
    intent_preds = None
    slot_preds = None

    for batch in tqdm(data_loader, desc='\033[93m' + "Statement Prediction: " + '\033[0m'):
        batch = tuple(t.to(device) for t in batch)
        with torch.no_grad():
            inputs = {"input_ids": batch[0],
                      "attention_mask": batch[1],
                      "intent_label_ids": None,
                      "slot_labels_ids": None}
            if args.model_type != "distilbert":
                inputs["token_type_ids"] = batch[2]
            outputs = model(**inputs)
            _, (intent_logits, slot_logits) = outputs[:2]

            # Intent Prediction
            if intent_preds is None:
                intent_preds = intent_logits.detach().cpu().numpy()
            else:
                intent_preds = np.append(intent_preds, intent_logits.detach().cpu().numpy(), axis=0)

            # Slot prediction
            if slot_preds is None:
                if args.use_crf:
                    # decode() in `torchcrf` returns list with best index directly
                    slot_preds = np.array(model.crf.decode(slot_logits))
                else:
                    slot_preds = slot_logits.detach().cpu().numpy()
                all_slot_label_mask = batch[3].detach().cpu().numpy()
            else:
                if args.use_crf:
                    slot_preds = np.append(slot_preds, np.array(model.crf.decode(slot_logits)), axis=0)
                else:
                    slot_preds = np.append(slot_preds, slot_logits.detach().cpu().numpy(), axis=0)
                all_slot_label_mask = np.append(all_slot_label_mask, batch[3].detach().cpu().numpy(), axis=0)

    intent_preds = np.argmax(intent_preds, axis=1)
    intent_preds_label = [intent_label_lst[intent_pred] for intent_pred in intent_preds]

    if not args.use_crf:
        slot_preds = np.argmax(slot_preds, axis=2)

    slot_label_map = {i: label for i, label in enumerate(slot_label_lst)}
    slot_preds_list = [[] for _ in range(slot_preds.shape[0])]

    for i in range(slot_preds.shape[0]):
        for j in range(slot_preds.shape[1]):
            if all_slot_label_mask[i, j] != pad_token_label_id:
                slot_preds_list[i].append(slot_label_map[slot_preds[i][j]])

    return lines, slot_preds_list, intent_preds_label


def get_model_args():
    init_logger()
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_dir", default="../../jointbert-model/web-gui-interactions", type=str,
                        help="Path to save, load model")
    parser.add_argument("--batch_size", default=32, type=int, help="Batch size for prediction")
    parser.add_argument("--no_cuda", default=True, action="store_true", help="Avoid using CUDA when available")
    pred_config = parser.parse_args()
    return pred_config
