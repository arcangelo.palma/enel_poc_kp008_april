import json
import logging
import os
import shutil
import sys
import urllib.error
import gc

from langdetect import detect_langs
from transformers import MarianTokenizer, MarianMTModel, AutoModelForSeq2SeqLM, AutoTokenizer
from tqdm import tqdm
from common import JavaProjectConfig
from utils import append_root_path, TColors

append_root_path()
# disable_all_logs()


def translation(lines, filename, li):
    translated = []
    print("{}Language Analysis of {}...{}".format(TColors.WARNING, filename, TColors.END_C))
    model_name = 'Helsinki-NLP/opus-mt-' + li + '-en'
    try:
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        model = AutoModelForSeq2SeqLM.from_pretrained(model_name)
    except ValueError:
        print("Load general model roa-en")
        tokenizer = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-roa-en')
        model = MarianMTModel.from_pretrained('Helsinki-NLP/opus-mt-roa-en')
    for line in tqdm(lines):
        translated_enc = model.generate(**tokenizer(line, return_tensors="pt", padding=True))
        translated_dec = [tokenizer.decode(t, skip_special_tokens=True) for t in translated_enc]
        translated.append(translated_dec[0] + " ")
    return translated


def preprocess_input():
    # load model for compute translation form romance language to English language
    input_dict = {}
    print("\n{}Language Analysis started...{}".format(TColors.WARNING, TColors.END_C))
    # model_name = 'Helsinki-NLP/opus-mt-roa-en'

    # file in argv to reading to convert
    file_path = os.path.join(TEST_CASES, sys.argv[1])
    with open(file_path, encoding='utf-8') as f:
        lines = f.read().split('\n')

    txt = " ".join(lines)
    text = ''.join(txt.split('"')[::2])

    li = detect_langs(text)
    li = str(li[0]).split(":")[0]
    print("{}(Done) Language Detection completed! Language: {}{}".format(TColors.OK_GREEN, li, TColors.END_C))
    filename = sys.argv[1].split('.')[0]
    if not li == "en":
        translated = translation(lines, filename, li)
    else:
        translated = lines
    print("{}(Done) Language Translation completed! {}".format(TColors.OK_GREEN, TColors.END_C))
    double_check = sys.argv[2]
    if "True" == double_check:
        translated = translation(translated, filename, 'roa')
        print("{}(Done) Language Correction completed! Language: en{}".format(TColors.OK_GREEN, TColors.END_C))

    original_translated_obj = {'original': lines, 'translation': translated}
    input_dict.update({filename: original_translated_obj})
    print("{}(Done) Language Analysis completed!{}".format(TColors.OK_GREEN, TColors.END_C))
    return input_dict


# create file json whit original and translation of text in English
def main():
    # reset java project
    # reset_java_project(JAVA_PROJECT_ROOT, JavaProjectConfig.JAVA_PROJECT_SRC, JavaProjectConfig.JAVA_PROJECT_BUILD)
    input_dict = preprocess_input()
    # refresh cache directory if already present
    if os.path.isdir(CACHE_DIR):
        shutil.rmtree(CACHE_DIR)
    os.mkdir(CACHE_DIR)
    with open(os.path.join(CACHE_DIR, 'tmp-dict.json'), 'w') as dict_file:
        json.dump(input_dict, dict_file)


if __name__ == '__main__':
    gc.collect()
    CONTENT_ROOT = '../../'
    CACHE_DIR = os.path.join(CONTENT_ROOT, "data/output/.cache")
    TEST_CASES = os.path.join(CONTENT_ROOT, 'data/input/jb-demo/')
    # path test file
    JAVA_PROJECT_ROOT = os.path.join(JavaProjectConfig.CONTENT_ROOT, JavaProjectConfig.JAVA_PROJECT_ROOT)
    main()
