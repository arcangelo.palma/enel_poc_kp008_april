import json
import logging
import os
import re
import shutil

import numpy as np
from langdetect import detect_langs
from transformers import MarianTokenizer, MarianMTModel, AutoModelForSeq2SeqLM, AutoTokenizer
from urlextract import URLExtract
from word2number import w2n

from common import JavaProjectConfig
from utils import append_root_path, TColors

append_root_path()
# disable_all_logs()

from src.graph2tree.config import get_args
from src.graph2tree.semantic import SemanticParserV2
from src.jointbert.predict import predict, get_model_args

icon_type = ['menu', 'download', 'upload', 'user', 'close', 'add', 'cart', 'favourite', 'like', 'unlike',
             'home', 'refresh', 'searchbar', 'search', "arrow", "arrow up", "arrow right", "arrow down",
             "arrow left"]


class FormatLineException(Exception):
    pass


def translator(originals):
    txt = " ".join(originals)
    text = ''.join(txt.split('"')[::2])

    li = detect_langs(text)
    li = str(li[0]).split(":")[0]
    model_name = 'Helsinki-NLP/opus-mt-en-' + li
    try:
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        model = AutoModelForSeq2SeqLM.from_pretrained(model_name)
    except ValueError:
        tokenizer = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-en-roa')
        model = MarianMTModel.from_pretrained('Helsinki-NLP/opus-mt-en-roa')
    return tokenizer, model


def aggregate_token_rl(original, tokens, slot, tokenizer, model):
    next = False
    flag = -1
    first_pos = -1
    # cerco il testo etichettato male non come object_label
    # sono etichettati come parte dell'elemento vicino
    elements = ["button", "input_field", "dropdown"]
    original = original + " "

    for t, s in zip(tokens, slot):
        if s != "O" and s.split('.')[-1] in elements and t != "input_field":
            if not next:
                next = True
                first_pos = tokens.index(t)
            elif ("label_" not in t or not t[0] == "\"") and not t[-1] == "\"":
                i = tokens.index(t)
                # t = t.lower()
                tokens[i] = "\"" + t + "\""
                old = original
                try:
                    translated_enc = model.generate(**tokenizer(t, return_tensors="pt", padding=True))
                    trad = [tokenizer.decode(to, skip_special_tokens=True) for to in translated_enc][0]  # .lower()
                    original = re.sub(r" " + trad + " ", " label_\"" + trad + "\" ", original)
                except ValueError:
                    continue

                if old != original:
                    slot[i] = "B-interface.object_label"
                    slot[first_pos] = "B-" + slot[first_pos].split("-")[-1]
                    flag = -1
                else:
                    original = re.sub(r" " + t + " ", " label_\"" + t + "\" ", original)
                    if old != original:
                        slot[i] = "B-interface.object_label"
                        slot[first_pos] = "B-" + slot[first_pos].split("-")[-1]
                        flag = -1
        else:
            next = False

        if "label_" in t and t[-1] == "\"":
            flag = tokens.index(t)

    if flag != -1 and slot[flag] != "B-interface.object_label":
        slot[flag] = "B-input.sequence"
        tokens[flag] = tokens[flag].split("label_")[-1]

    return original, tokens[::-1], slot[::-1]


def aggregate_token_lr(original, tokens, slot, tokenizer, model):
    first_pos = -1
    idx = []
    original = original + " "

    # ripulisco unendo eventuali sequenze di label
    for t, s in zip(tokens, slot):
        if s != "O" and s.split('.')[-1] == "object_label" and "label_" not in t:
            if first_pos == -1:
                first_pos = tokens.index(t)
            else:
                idx.append(tokens.index(t))

    if first_pos != -1:
        for i in range(len(idx)):
            t = tokens[idx[i]]
            if ("label_" not in t and not t[0] == "\"") and not t[-1] == "\"":
                tokens[first_pos] += " " + t
                del tokens[idx[i]]
                del slot[idx[i]]

        # t = tokens[first_pos].lower()

        if ("label_" not in t and not t[0] == "\"") and not t[-1] == "\"":
            tokens[first_pos] = "\"" + t + "\""
            old = original
            try:
                translated_enc = model.generate(**tokenizer(t, return_tensors="pt", padding=True))
                trad = [tokenizer.decode(to, skip_special_tokens=True) for to in translated_enc][0]  # .lower()
                original = re.sub(r" " + trad + " ", " label_\"" + trad + "\" ", original)
            except ValueError:
                print("Text cleaning ...")

            if old == original:
                original = re.sub(r" " + t + " ", " label_\"" + t + "\" ", original)
                if old == original:
                    raise FormatLineException(
                        "incorrect input in the lines (use \" \" for sequence or label) in: " + original)
    return original, tokens, slot


# convert slot list into json
def slots_to_json(originals, lines, slots):
    json_slots_list = []
    url_extractor = URLExtract()
    tokenizer, model = translator(originals)
    labels = ["sequence", "object_label"]
    count = 1
    for original, tokens, slot in zip(originals, lines, slots):
        tokens = tokens.split()
        next = False
        for t in tokens:
            i = tokens.index(t)
            if slot[i].split(".")[-1] == "sequence" and not t[0] == "\"" and not t[-1] == "\"":
                slot[i] = 'O'
            elif slot[i].split("-")[-1] == "interface.input_field" and not t == "input_field":
                if slot[i - 1].split(".")[-1] in labels or (
                        i < len(tokens) - 1 and slot[i + 1].split(".")[-1] in labels):
                    if slot[i] == "I-interface.input_field":
                        slot[i] = "I-interface.object_label"
                elif next:
                    slot[i] = "I-interface.object_label"
                else:
                    slot[i] = "B-interface.object_label"
                    next = True
            else:
                next = False

        # original = original.lower()
        original, tokens, slot = aggregate_token_rl(original, tokens[::-1], slot[::-1], tokenizer, model)
        original, tokens, slot = aggregate_token_lr(original, tokens, slot, tokenizer, model)
        json_slots = {'data': {}}
        # extract for every line object and related action
        action = find_slots_action(tokens, slot)
        obj = find_slots_gui_object(tokens, slot)
        label, sequence, idx = find_slots_gui_object_label(original, tokens, slot)

        if ("in" in tokens or "within" in tokens) and action == "typing":
            if obj['object'] is None and len(label) == 0 and len(sequence) == 2:
                if "in" in tokens:
                    i = tokens.index("in") + 1
                else:
                    i = tokens.index("within") + 1
                while i < len(tokens) and not (
                        slot[i].split(".")[-1] == "object_label" or slot[i].split(".")[-1] == "sequence"):
                    i += 1

                if i < len(tokens):
                    obj['object'] = "input_field"
                    label.append(sequence[-1])
                    sequence = sequence[:-1]
                    slot[i] = "B-interface.object_label"
                else:
                    raise FormatLineException(
                        "incorrect input in the " + str(count) + "° line (insert more element)")
            else:
                if "in" in tokens:
                    i = tokens.index("in") - 1
                else:
                    i = tokens.index("within") - 1
                while i >= 0 and not slot[i].split(".")[-1] == "object_label":
                    i -= 1
                if i >= 0:
                    sequence.append(label[0])
                    label = label[1:]
                    slot[i] = "B-input.sequence"

        inputs = find_slots_input(tokens, slot)

        if action is not None:
            json_slots['id'] = action
        else:
            json_slots['id'] = "ifany"
        if obj['object'] is not None:
            # detect type of element if present (i.e. radio button ecc...)
            if 'type' in obj:
                json_slots['data']['type'] = obj['type']
            if 'position' in obj:
                json_slots['data']['position'] = obj['position']
                if 'position_object' in obj:
                    p = tokens.index(obj['position_object'])
                    json_slots['data']['position_object'] = extract_original_input(tokens, original, idx.index(p),
                                                                                   ids_of_interest=p)
            json_slots['data']['object'] = obj['object']
        else:
            if action == "visiting":
                json_slots['data']['object'] = "url"
            elif action == "pressure" or action == "selection":
                if "B-input.url" in slot:
                    json_slots['data']['object'] = "url"
                    json_slots['data']['type'] = "link"
                else:
                    json_slots['data']['object'] = "button"
        if label:
            if "ifany" in json_slots['id']:
                json_slots['data']['sequence'] = label[0]
                json_slots['data']['object'] = "label"
            else:
                json_slots['data']['label'] = label[0]

        if sequence:
            if not action == "scroll":
                json_slots['data']['sequence'] = sequence[0]
            else:
                json_slots['data']['label'] = sequence[0]

        if inputs:
            if action == "scroll" and obj['object'] is None:
                json_slots['data']['length'] = inputs['instance_number']
            else:
                if 'instance_number' in inputs.keys():
                    json_slots['data']['instanceNumber'] = inputs['instance_number']
                elif 'option_number' in inputs.keys():
                    json_slots['data']['option_number'] = inputs['option_number']

        # extract url
        if "id" in json_slots:
            if json_slots['id'] == 'visiting' or "B-input.url" in slot:
                url = url_extractor.find_urls(original)[0]
                if "http://" not in url and "https://" not in url:
                    url = "http://" + url
                json_slots['data']['sequence'] = url

        json_slots_list.append(json_slots)
        count += 1
    return json_slots_list


def find_slots_action(tokens, slots):
    for t, s in zip(tokens, slots):
        if s != 'O' and s.split('-')[1].split('.')[0] == 'interaction':
            return s.split('.')[-1]
    if "B-input.url" in slots and "B-interface.url" in slots:
        return "visiting"
    return None


def find_slots_gui_object_label(original, tokens, slots):
    label = []
    ids = []

    for i, obj in enumerate(zip(tokens, slots)):
        t, s = obj
        if s != 'O' and s.split('.')[-1] == 'object_label':
            label.append("label")
            ids.append(i)
        if s != 'O' and s.split('.')[-1] == 'sequence' and (t[0] == "\"" and t[-1] == "\""):
            label.append("sequence")
            ids.append(i)
    original_label = []
    original_sequence = []

    # pos = token position
    # i = i° element -> i° text label in original text
    for pos, i in enumerate(ids):
        if label[pos] == "label":
            original_label.append(extract_original_input(tokens, original, pos, ids_of_interest=i))
        else:
            original_sequence.append(extract_original_input(tokens, original, pos, ids_of_interest=i))
    return original_label, original_sequence, ids


def extract_original_input(tokens, original, position, ids_of_interest):
    offset = len(' '.join(tokens[:ids_of_interest])) + 1

    marker_pos_translated = [i for i, letter in enumerate(' '.join(tokens)) if letter == '\"']
    marker_pos_translated = [o + offset for o in marker_pos_translated]
    marker_pos_original = [i for i, letter in enumerate(original) if letter == '\"']
    start_end_label_id = []
    for translated_marker_idx in marker_pos_translated:
        marker_pos_original_offset = [abs(marker_pos - translated_marker_idx)
                                      for marker_pos in marker_pos_original]
        start_index = np.argmin(np.array(marker_pos_original_offset))
        start_end_label_id.append(marker_pos_original[start_index])
        del marker_pos_original[start_index]

    start_end_label_id = sorted(start_end_label_id)
    try:
        return original[(start_end_label_id[2 * position]) + 1:start_end_label_id[(2 * position) + 1]]
    except IndexError:
        raise FormatLineException("incorrect input in the lines (use \" \" for sequence or label) in: " + original)


def find_slots_gui_object(tokens, slots):
    gui_object = {'object': None}
    interactable = ["url", "button", "dropdown", "icon", "input_field"]
    not_pos = ["in", "on", "out"]
    for t, s in zip(tokens, slots):
        if s != 'O' and s.split('-')[1].split('.')[0] == 'interface':
            suffix = s.split('.')[-1]
            if gui_object['object'] is None and suffix in interactable:
                gui_object['object'] = suffix
            elif gui_object[
                'object'] is not None and 'position' in gui_object.keys() and 'position_object' not in gui_object.keys():
                gui_object['position_object'] = t
            if gui_object['object'] == "button" and t.split("_")[-1] == "button":
                gui_object['type'] = t.split("_button")[0]
            elif gui_object['object'] == "icon":
                if t.split("_icon")[0] in icon_type:
                    gui_object['type'] = t.split("_icon")[0]
                else:
                    gui_object['object'] = "button"
            elif suffix == "position" and t not in not_pos:
                gui_object['position'] = t
            elif suffix == "url":
                if 'url' in t or 'page' in t or 'site' in t:
                    continue
                elif 'link' in t:
                    gui_object['type'] = 'link'

        if s != 'O' and s.split('-')[1].split('.')[0] == 'quantification':
            gui_object['quantification'] = t
    return gui_object


def find_slots_input(tokens, slots):
    input_value = {}

    for i, obj in enumerate(zip(tokens, slots)):
        t, s = obj
        if s != 'O' and s.split('-')[1].split('.')[0] == 'input':
            if s.split('.')[-1] == 'instance_number':
                input_value = convert_input_number(t, True, False)
            elif s.split('.')[-1] == 'option_number':
                input_value = convert_input_number(t, False, True)
    return input_value


def convertitor(t):
    if "first" == t:
        return 1
    elif "second" == t:
        return 2
    elif "third" == t:
        return 3
    elif "fourth" == t:
        return 4
    elif "fifth" == t:
        return 5
    else:
        return w2n.word_to_num(t)


def convert_input_number(t, position, option):
    lista = {}
    if "number" not in t:
        try:
            if position:
                lista["instance_number"] = convertitor(t)
            elif option:
                t = t.replace("option_", "")
                lista["option_number"] = convertitor(t)
        except ValueError:
            logging.info("{}Error during conversion of number!!{}".format(TColors.FAIL, TColors.END_C))
    return lista


def open_dict(path):
    with open(path) as file:
        data = json.load(file)
    return data


def extract_conditions(lines, intents):
    conditions = []
    for line, i in zip(lines, intents):
        if i == 'test_web_condition':
            conditions.append(line)
    return conditions


def save_conditions_for_inference(conditions, path="../../data/graph2tree/infer/raw/test.txt"):
    with open(path, 'w') as mcf:
        conditions = ["{}\ttbd".format(c) for c in conditions]
        mcf.write('\n'.join(conditions))


def predict_conditions(mode='web'):
    runner = SemanticParserV2(opt=get_args(mode))
    generated_conditions = runner.infer()
    condition_json = dict()
    for i, gc in enumerate(generated_conditions):
        condition_json.update({str(i): gc})
    folder_to_remove = get_args(None)['graph_construction_args']['graph_construction_share']['inference_root_dir']
    shutil.rmtree(os.path.join(folder_to_remove, 'processed'))
    return condition_json


def post_process_cv_input(lines, json_array, gen_conditions):
    cond_n = 0
    if gen_conditions:
        for line, json_obj in zip(lines, json_array):
            if json_obj['id'] == 'ifany':
                condition = gen_conditions[str(cond_n)]
                cond_n += 1
                if 'invisible' in condition and " not " in line:
                    json_obj['id'] = 'ifnotany'


def main():
    # load translated json
    with open(os.path.join(CACHE_DIR, 'tmp-dict.json'), 'r') as dict_file:
        input_dict = json.load(dict_file)

        for test, actions in input_dict.items():
            # predict
            print("{}Analyzing {} text data for computer vision...{}".format(TColors.WARNING, test, TColors.END_C))
            translated = actions['translation']
            original = actions['original']

            # chunking of command in raw text (IOB2 notation)
            lines, slots, intents = predict(CONTENT_ROOT, translated, get_model_args())
            i = 0
            # extract lines that contains conditions
            for line, intent, slot in zip(lines, intents, slots):
                # print(line)
                # print(intent)
                # print(slot)

                for t, s in zip(line.split(), slot):
                    if t == "label_empty" or t == "empty":
                        slot[line.split().index(t)] = 'O'
                        lines[i] = line.replace(" label_empty", " empty")
                i += 1

            conditions = extract_conditions(lines, intents)
            generated_conditions = dict()
            if conditions:
                save_conditions_for_inference(conditions)
                generated_conditions = predict_conditions()
            try:
                cv_input_data = slots_to_json(original, lines, slots)
                post_process_cv_input(lines, cv_input_data, generated_conditions)

                print("{}(Done) Data of {} are ready for computer vision.\nText analysis completed! {}"
                      .format(TColors.OK_GREEN, test, TColors.END_C))
                with open(os.path.join(CV_INPUT_DIR, "{}.analyzed.json".format(test)), 'w') as cv_file, \
                        open(os.path.join(CV_OUTPUT_DIR, "{}.conditions.json".format(test)), 'w') as cond_file:
                    json.dump(cv_input_data, cv_file, indent=4)
                    json.dump(generated_conditions, cond_file, indent=4)
            except FormatLineException as err:
                print(TColors.FAIL + str(err) + TColors.END_C)


if __name__ == '__main__':
    CONTENT_ROOT = '../../'
    CV_INPUT_DIR = '../test/input/'
    CV_OUTPUT_DIR = '../test/output/'
    CACHE_DIR = os.path.join(CONTENT_ROOT, "data/output/.cache")
    DICT_FOLDER = os.path.join(CONTENT_ROOT, 'data/input/jb-demo/dicts/')
    JAVA_PROJECT_ROOT = os.path.join(JavaProjectConfig.CONTENT_ROOT, JavaProjectConfig.JAVA_PROJECT_ROOT)
    main()
