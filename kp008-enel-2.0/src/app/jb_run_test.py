import os
import sys
from utils import append_root_path, disable_all_logs, TColors

append_root_path()
# disable_all_logs()

from src.java_generator.builder.JavaBuilder import StatementsBuilder
from common import compile_and_run, JavaProjectConfig


def main():
    # compiling and running
    print("{}Run source code of {}...{}".format(TColors.WARNING, sys.argv[1], TColors.END_C))
    compile_and_run(os.path.join(JavaProjectConfig.CONTENT_ROOT, JavaProjectConfig.JAVA_PROJECT_ROOT),
                    JavaProjectConfig.JAVA_PROJECT_SRC,
                    JavaProjectConfig.JAVA_PROJECT_BUILD,
                    JavaProjectConfig.JAVA_PROJECT_LIB,
                    StatementsBuilder.PACKAGE,
                    file_to_run=sys.argv[1],
                    compile=sys.argv[2],
                    run=True)
    print("{}(Done) Source code run of {} completed!{}".format(TColors.OK_GREEN, sys.argv[1], TColors.END_C))

if __name__ == '__main__':
    main()
