import glob
import os
import shutil


class JavaProjectConfig:
    CONTENT_ROOT = "../../"
    JAVA_PROJECT_ROOT = "java_test_project"
    JAVA_PROJECT_SRC = "src"
    JAVA_PROJECT_LIB = "lib"
    JAVA_PROJECT_BUILD = "build"
    OUTPUT_FOLDER = "data/output"
    CLASS_TO_EXCLUDE = ['EventCapture.java', 'TestOperations.java']


def reset_java_project(root, src, build):
    class_to_remove = [file for file in list(set(os.listdir(os.path.join(root, src)))
                                             .difference(set(JavaProjectConfig.CLASS_TO_EXCLUDE)))]
    for ctr in class_to_remove:
        os.remove(os.path.join(root, src, ctr))
    if os.path.exists(os.path.join(root, build)):
        shutil.rmtree(os.path.join(root, build))

    os.makedirs(os.path.join(root, build))


def compile_and_run(root, src, build, lib, package, file_to_run=None, compile=True, run=True):
    initial_path = os.getcwd()
    os.chdir(root)
    package_dir = package.split(" ")[-1].split(";")[0]
    # tests_to_run = [file.split(".")[0] for file in
    #                 list(set(os.listdir(src)).difference(set(JavaProjectConfig.CLASS_TO_EXCLUDE)))]
    if compile:
        os.system('javac -cp ".:./{}/*" {}/{}.java -d {}'.format(lib, src, file_to_run, build))
        print('javac -cp ".:./{}/*" {}/{}.java -d {}'.format(lib, src, file_to_run, build))

    os.chdir("{}".format(build))
    output_file_data = []
    if run:
        os.system('java -cp ".:../{}/*" org.junit.runner.JUnitCore {}.{}'.format(lib, package_dir, file_to_run))
        print('java -cp ".:../{}/*" org.junit.runner.JUnitCore {}.{}'.format(lib, package_dir, file_to_run))
        # moving output files to output directory
        # os.chdir('..')
        # os.chdir('..')
        for output_file in glob.glob(os.path.join(root, build, '*.txt')):
            filename = output_file.split("/")[-1]
            with open(output_file, 'r') as of:
                actions_list = of.readlines()
            output_dict = dict()
            output_dict.update({"test": filename, "actions": actions_list})
            output_file_data.append(output_dict.copy())
            shutil.move(output_file, os.path.join(JavaProjectConfig.CONTENT_ROOT,
                                                  JavaProjectConfig.OUTPUT_FOLDER,
                                                  filename))
    os.chdir(initial_path)

    return output_file_data
