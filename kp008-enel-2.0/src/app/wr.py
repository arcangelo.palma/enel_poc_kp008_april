import io
import json
from typing import Dict, Any, Union, List


def read_json(path) -> Union[Dict[str, Any], List[Dict[str, Any]]]:
    with io.open(path, mode="r", encoding='utf-8-sig') as f_p:
        return json.load(f_p)


def write_json(path_out, json_ob):
    with io.open(path_out, mode="w", encoding='utf-8-sig') as f_p:
        return json.dump(json_ob, f_p)
