import os
import sys

from utils import append_root_path, TColors

append_root_path()
# disable_all_logs()


from webdriver_manager.chrome import ChromeDriverManager
from src.app.common import JavaProjectConfig
from src.app.wr import read_json
from src.java_generator.builder.JavaBuilder import JavaTestBuilder


def main():
    driver_path = ChromeDriverManager().install()
    print("{}Building source code of {}...{}".format(TColors.WARNING, sys.argv[1], TColors.END_C))
    resolved_dict = read_json(os.path.join(OUTPUT_DIR, "{}.resolved.json".format(sys.argv[1])))
    resolved_cond = read_json(os.path.join(OUTPUT_DIR, "{}.conditions.json".format(sys.argv[1])))
    builder = JavaTestBuilder(class_name=sys.argv[1],
                              folder=os.path.join(JAVA_PROJECT_ROOT, JavaProjectConfig.JAVA_PROJECT_SRC),
                              driver_path=driver_path)
    builder.build(json_mapping=resolved_dict, json_conditions=resolved_cond)
    builder.persist()
    print("{}(Done) Source code build of {} completed!{}".format(TColors.OK_GREEN, sys.argv[1], TColors.END_C))


if __name__ == "__main__":
    OUTPUT_DIR = "../test/output/"
    JAVA_PROJECT_ROOT = os.path.join(JavaProjectConfig.CONTENT_ROOT, JavaProjectConfig.JAVA_PROJECT_ROOT)
    main()
