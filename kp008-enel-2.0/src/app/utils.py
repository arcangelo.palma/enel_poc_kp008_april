import sys
import os
CALLED = False


class TColors:
    OK_GREEN = '\033[92m'
    WARNING = "\033[93m"
    FAIL = '\033[91m'
    END_C = '\033[0m'


def disable_all_logs():
    global CALLED
    if not CALLED:
        sys.stderr = open(os.devnull, 'w')
        CALLED = True


def append_root_path():
    import sys
    sys.path.append("/home/kebula-titan/PycharmProjects/kp008-enel/enel_poc_kp008_april/kp008-enel-2.0")


def print_jb_pipeline(lines, slots, intents):
    for line, s, i in zip(lines, slots, intents):
        print(line)
        print(s)
        print(i)
        print("-"*15)
