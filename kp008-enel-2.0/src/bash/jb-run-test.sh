#!/bin/bash
alias py="/home/kebula-titan/anaconda3/envs/nlp-test-case-generation/bin/python"
file_name=$1
browser=$2
java_file="../../java_test_project/src/$file_name.java"
if [ -f "$java_file" ]; then
  sh ./jb-run.sh "$file_name"
else
  sh ./jb-vision.sh "$file_name" "$browser"
  sh ./jb-build.sh "$file_name"
  sh ./jb-run.sh "$file_name"
fi
