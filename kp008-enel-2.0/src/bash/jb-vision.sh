#!/bin/bash
file=$1
browser=$2
alias py="/home/kebula-titan/anaconda3/envs/proxyENEL/bin/python"
actions="../test/input/${file}.analyzed.json"
resolved="../test/output/${file}.resolved.json"
cd ../demo_personal/yolov5
py ../application.py --json_file "$actions" --json "$resolved" --browser "$browser"
