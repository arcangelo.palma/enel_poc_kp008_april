#!/bin/bash
alias py="/home/kebula-titan/anaconda3/envs/nlp-test-case-generation/bin/python"
file_name=$1
browser=$2
verbose=$3
check="False"

java_file="../../java_test_project/src/$file_name.java"
if [ -f "$java_file" ]; then
  py ../app/jb_run_test.py "$file_name" false
else
  py ../app/jb_translate_test.py "$file_name.txt" "$check"
  py ../app/jb_prepare_test.py
  actions="../test/input/$file_name.analyzed.json"
  resolved="../test/output/$file_name.resolved.json"
  if  [ -f "$actions" ]; then
    cd ../demo_personal/yolov5
    py ../application.py --json_file "$actions" --json "$resolved" --browser "$browser" --verbose "$verbose"
    cd ..
  fi
  if  [ -f "$resolved" ]; then
    py ../app/jb_build_test.py "$file_name"
    if [ -f "$java_file" ]; then
      py ../app/jb_run_test.py "$file_name" true
    fi
  fi
fi
