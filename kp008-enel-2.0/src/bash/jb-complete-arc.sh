#!/bin/bash
browser=$1
verbose=$2
export PYTHONPATH=$PYTHONPATH:/home/kebula-titan/PycharmProjects/kp008-enel-2.0/src/demo_personal/yolov5
alias py="/home/kebula-titan/anaconda3/envs/nlp-test-case-generation/bin/python"

sh ./jb-generation.sh "EnelTestFailure" "$browser" "$verbose" ########TEST OK
sh ./jb-generation.sh "EnelTestFailureEs" "$browser" #######TEST OK
sh ./jb-generation.sh "EnelTestFailureEn" "$browser" ######TEST OK
sh ./jb-generation.sh "WixFormTest" "$browser" "$verbose" ########TEST OK
sh ./jb-generation.sh "StrumentiMus" "$browser" "$verbose" #######TEST OK
sh ./jb-generation.sh "AmazonRegister" "$browser" "$verbose" #####TEST  OK
#sh ./jb-generation.sh "NewEnelTest" "$browser" "$verbose" ########TEST SEMI OK
#sh ./jb-generation.sh "MozzarellaTest" "$browser" "$verbose"
#sh ./jb-generation.sh "GrouponTest" "$browser" "$verbose"

exit 1