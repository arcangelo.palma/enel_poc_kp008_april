from src.java_generator.parser.JavaParserListener import JavaParserListener
from webdriver_manager.chrome import ChromeDriverManager


def get_original_text(ctx):
    input_stream = ctx.start.getInputStream()
    start = ctx.start.start
    stop = ctx.stop.stop
    return input_stream.getText(start, stop)


class Statements:
    IMPORT = "import org.openqa.selenium.support.events.EventFiringWebDriver;\n" \
             "import java.io.FileWriter;\n"
    DRIVER = "\nSystem.setProperty(\"webdriver.chrome.driver\", \"{}\");"
    HANDLER = "private EventFiringWebDriver eventHandler;\n" \
              "private EventCapture eCapture;"
    BEFORE = "eventHandler = new EventFiringWebDriver(driver);\n" \
             "eCapture = new EventCapture();\n" \
             "eventHandler.register(eCapture);\n"
    AFTER = "FileWriter writer = new FileWriter(this.getClass().getName()+\".txt\");\n" \
            "int actionId = 0;\n" \
            "for(String action: eCapture.actions) {\n" \
            "actionId +=1 ;\n" \
            "writer.write(actionId+\"\t-\t\"+action + System.lineSeparator());\n" \
            "}\n" \
            "writer.close();\n" \
            "eventHandler.unregister(eCapture);\n"
    DEFAULT_PACKAGE = "package src;\n"
    TRANSLATED_OP_CLASS = "TestOperations"
    THROWS = "throws Exception"
    ASSERTION = "assertTrue({});"
    VALUE_TYPE = "double"
    STORE_VALUE = "{} = Double.parseDouble(eventHandler.findElement(By.xpath(\"{}\")).getAttribute(\"value\"));"
    RESULT_XPATH = "/html/body/div[1]/div[3]/form/input[1]"


class JavaReader(JavaParserListener):
    WEB_DRIVER_REPLACER = 'eventHandler'

    def __init__(self):
        self._package = Statements.DEFAULT_PACKAGE
        self._imports = Statements.IMPORT.split('\n')
        self._class = None
        self._curr_modifier = None
        self._instance_variables = Statements.HANDLER.split('\n')
        self._inside_methods = False
        self._curr_annotation = None
        self._curr_method = None
        self._methods = []
        self._web_driver_name = None
        self._test_method = None
        self._declared_vars = []

    # def enterPackageDeclaration(self, ctx):
    #     # self._package = get_original_text(ctx)
    #     self._package = Statements.DEFAULT_PACKAGE

    def enterImportDeclaration(self, ctx):
        self._imports.append(get_original_text(ctx))

    def enterClassDeclaration(self, ctx):
        self._class = get_original_text(ctx.parentCtx).split("{")[0]

    def enterMethodDeclaration(self, ctx):
        self._curr_method = get_original_text(ctx)
        if self.process_method():
            self._methods.append(self._curr_method)
        self._curr_annotation = None
        self._inside_methods = True

    def exitMethodDeclaration(self, ctx):
        self._inside_methods = False

    def enterModifier(self, ctx):
        self._curr_modifier = get_original_text(ctx)

    def enterVariableDeclarator(self, ctx):
        if not self._inside_methods:
            var = get_original_text(ctx.parentCtx.parentCtx)
            if 'WebDriver' in var:
                self._web_driver_name = var.split('WebDriver')[-1].replace(';', '').strip()
            self._instance_variables.append("{} {}".format(self._curr_modifier, var))

    def enterAnnotation(self, ctx):
        self._curr_annotation = get_original_text(ctx)

    def process_method(self):
        append = True
        if self._curr_annotation is None:
            self._curr_method = "{} {}\n".format(self._curr_modifier, self._curr_method)
        elif "before" in self._curr_annotation.lower():
            self.process_before_method()
        elif "test" in self._curr_annotation.lower():
            self.process_test_method()
            append = False
        elif "after" in self._curr_annotation.lower():
            self.process_after_method()
        self._format_method()
        return append

    def _format_method(self):
        method = [line.strip() for line in self._curr_method.split("\n")]
        method = '\n'.join(method)
        method = method.split("{")
        method_sign = method[0].strip()
        method = '{'.join(method[1:])
        method = method.rsplit("}", 1)[0]
        method = method.split('\n')
        method = method[:len(method) - 1]
        method = ["{}{}".format('\t', line) for line in method]
        method = '\n'.join(method)
        method = "{}{{{}\n}}\n".format(method_sign, method)
        self._curr_method = method

    def _verify_throw_statement(self):
        sign = self._curr_method.split("{")[0]

        if "throws" in sign:
            exceptions_list = sign.split("throws")[-1].split(",")
            for exception in exceptions_list:
                # if Exception is present there is no need to add it
                # otherwise all the other exceptions ar replaced with Exception
                if exception.strip() == "Exception":
                    return

        # end of method declaration
        sign = "{}) {}".format(sign.split(")")[0], Statements.THROWS)
        body = self._curr_method.split("{")[1:]
        body = '{'.join(body)

        self._curr_method = "{} {{{}".format(sign, body)

    def process_before_method(self):
        self._curr_method = self._curr_method.rsplit('}', 1)[0]
        sign = self._curr_method.split("{")[0]
        body = self._curr_method.split("{")[1:]
        body = '{'.join(body)
        driver = Statements.DRIVER.format(ChromeDriverManager().install())
        body = "{}{}".format(driver, body)
        self._curr_method = "{".join([sign, body])
        self._curr_method = "{}\n{} {}{}}}\n".format(self._curr_annotation,
                                                     self._curr_modifier,
                                                     self._curr_method,
                                                     Statements.BEFORE)
        self._verify_throw_statement()

    def process_after_method(self):
        self._curr_method = self._curr_method.rsplit('}', 1)[0]
        self._curr_method = "{}\n{} {}{}}}\n".format(self._curr_annotation,
                                                     self._curr_modifier,
                                                     self._curr_method,
                                                     Statements.AFTER)

        self._verify_throw_statement()

    def process_test_method(self):
        self._verify_throw_statement()
        self._curr_method = "{}\n{} {}".format(self._curr_annotation,
                                               self._curr_modifier,
                                               self._curr_method.replace(self._web_driver_name,
                                                                         JavaReader.WEB_DRIVER_REPLACER))
        self._verify_throw_statement()

        self._format_method()
        self._test_method = self._curr_method

    def add_conditions(self, actions, conditions, predictions):
        test_method = '}'.join('{'.join(self._test_method.split('{')[1:]).split('}')[0:-1]).split('\n')
        new_test_method = []
        test_method = [st for st in test_method if len(st.replace('\t', '')) > 0]
        for action in actions:
            action = action.replace('\n', '')
            if action not in conditions:
                selector_attr, selector_val = action.split(' ')[-1].split('=')
                selector_attr = selector_attr.strip().replace('\n', '')
                selector_val = selector_val.strip().replace('\n', '')
                statement = "By.{}(\"{}\")".format(selector_attr, selector_val)
                while len(test_method) > 0:
                    tm = test_method.pop(0)
                    new_test_method.append(tm)
                    if statement in tm:
                        break
            else:
                # input binding
                var_name = 'R'
                statement = Statements.STORE_VALUE.format(var_name, Statements.RESULT_XPATH)
                if var_name not in self._declared_vars:
                    self._declared_vars.append(var_name)
                    statement = "\t{} {}".format(Statements.VALUE_TYPE, statement)
                else:
                    statement = "\t{}".format(statement)
                new_test_method.append(statement)
                # conditions insertion
                pt = predictions.pop(0)
                pt = pt.replace("_", "{}.".format(Statements.TRANSLATED_OP_CLASS)) \
                    .replace(" ", "") \
                    .replace("\n", "")
                pt = Statements.ASSERTION.format(pt)
                new_test_method.append("\t{}".format(pt))
        method_sign = self._test_method.split("{")[0]
        self._test_method = "{}{{\n{}\n}}\n".format(method_sign, '\n'.join(new_test_method))

    @property
    def full_code(self):
        return "{}\n\n{}\n\n{} {{\n{}\n\n{}\n{}}}".format(self._package,
                                                          "\n".join(self._imports),
                                                          self._class.strip(),
                                                          "\n".join(self._instance_variables),
                                                          "\n".join(self._methods),
                                                          self._test_method)

    @property
    def class_name(self):
        return self._class.strip()

    @property
    def package(self):
        return self._package