import os
import urllib.parse as parse

from urlextract import URLExtract

from src.graph2tree.config import get_args
from src.graph2tree.semantic import SemanticParserV2
from src.java_generator.reader.JavaReader import Statements as G2TStatements


class StatementsBuilder:
    PACKAGE = "package src;"
    IMPORTS = ['import org.junit.Test;',
               'import org.junit.Before;',
               'import org.junit.After;',
               'import org.openqa.selenium.*;',
               'import org.openqa.selenium.chrome.ChromeDriver;',
               'import org.openqa.selenium.support.ui.Select;',
               'import org.openqa.selenium.remote.DesiredCapabilities;',
               'import static org.testng.Assert.assertTrue;']
    DRIVER_DECLARATION = 'System.setProperty("webdriver.chrome.driver", \"{}\");'
    SET_UP = ['DesiredCapabilities capabilities = new DesiredCapabilities();',
              'capabilities.setAcceptInsecureCerts(true);',
              'driver = new ChromeDriver(capabilities);',
              'driver.manage().window().maximize();',
              'jse = (JavascriptExecutor) driver;']
    TEAR_DOWN = ['driver.quit();']
    TOOLBAR_HEIGHT_INSTANCE_VAR = 'private static final double TOOLBAR_HEIGHT = 60;'
    COMMON_INSTANCE_VARS = ['private WebDriver driver;',
                            'private JavascriptExecutor jse;']
    SET_UP_ANNOTATIONS = ['@Before']
    TEAR_DOWN_ANNOTATIONS = ['@After']
    TEST_ANNOTATIONS = ['@Test']

    OPEN_LINK = 'driver.get("{}");'
    FIND_ELEMENT = 'driver.findElement(By.xpath("{}"))'
    CLICK = 'click()'
    SEND_KEYS = 'sendKeys("{}")'
    SELECT = 'new Select({})'
    SELECT_BY_INDEX = 'selectByIndex({})'

    STORE_VALUE = '{} = Double.parseDouble(driver.findElement(By.xpath(\"{}\")).getAttribute(\"value\"));'

    REMOVE_COOKIE_BANNER = ['JavascriptExecutor js = (JavascriptExecutor) driver;',
                            'js.executeScript("return document.getElementById(\'consent_blackbar\').remove();");']

    CLICK_WRAPPER = "this.clickWrapper(By.xpath(\"{}\"), {}, {}, {});"
    # CLICK_WRAPPER_METHOD = "private void clickWrapper(By by, boolean inOverlay, int x, int y) {\n" \
    #                        "boolean flag = true;\n" \
    #                        "while (flag){\n" \
    #                        "try {\n" \
    #                        "driver.findElement(by).click();\n" \
    #                        "flag = false;\n" \
    #                        "} catch (ElementClickInterceptedException e) {\n" \
    #                        "WebElement element = driver.findElement(by);\n" \
    #                        "if(!inOverlay) {\n" \
    #                        "int elementYPosition = element.getLocation().y;\n" \
    #                        "double currentYPosition = Double.parseDouble(jse.executeScript(\"return window.scrollY\").toString());\n" \
    #                        "double scrollToPerform = elementYPosition - currentYPosition - TOOLBAR_HEIGHT;\n" \
    #                        "jse.executeScript(\"window.scrollBy(0, \"+scrollToPerform+\")\");\n" \
    #                        "} else {\n" \
    #                        "jse.executeScript(\"arguments[0].scrollIntoView(true);\", element);\n" \
    #                        "}\n" \
    #                        "} catch (NoSuchElementException e) {\n" \
    #                        "if(!inOverlay) {\n" \
    #                        "double currentYPosition = Double.parseDouble(jse.executeScript(\"return window.scrollY\").toString());\n" \
    #                        "double scrollToPerform = y - currentYPosition - TOOLBAR_HEIGHT;\n" \
    #                        "jse.executeScript(\"window.scrollBy(0, \"+scrollToPerform+\")\");\n" \
    #                        "} else{\n" \
    #                        "Double currY = (Double) jse.executeScript(\"return document.elementsFromPoint(0,0)[0].scrollTop\");" \
    #                        "jse.executeScript(\"document.elementFromPoint(0,0).scrollBy(0, \"+(320-currY)+\");\");\n" \
    #                        "jse.executeScript(\"var ev = new MouseEvent('click', {'view': window," \
    #                        "'bubbles': true," \
    #                        "'cancelable': true," \
    #                        "'screenX': \"+x+\"," \
    #                        "'screenY': \"+y+\"" \
    #                        "});" \
    #                        "e=document.elementFromPoint(\"+x+\",\"+y+\");e.dispatchEvent(ev);\");\n" \
    #                        "flag=false;\n" \
    #                        "}\n" \
    #                        "} catch (StaleElementReferenceException | ElementNotInteractableException ignored) {\n" \
    #                        "}\n" \
    #                        "}\n" \
    #                        "}"
    CLICK_WRAPPER_METHOD = "private void clickWrapper(By by, boolean inOverlay, int x, int y) {\n" \
                           "boolean flag = true;\n" \
                           "while (flag){\n" \
                           "try {\n" \
                           "driver.findElement(by).click();\n" \
                           "flag = false;\n" \
                           "} catch (ElementClickInterceptedException e) {\n" \
                           "WebElement element = driver.findElement(by);\n" \
                           "if(!inOverlay) {\n" \
                           "int elementYPosition = element.getLocation().y;\n" \
                           "double currentYPosition = Double.parseDouble(jse.executeScript(\"return window.scrollY\").toString());\n" \
                           "double scrollToPerform = elementYPosition - currentYPosition - TOOLBAR_HEIGHT;\n" \
                           "jse.executeScript(\"window.scrollBy(0, \"+scrollToPerform+\")\");\n" \
                           "} else {\n" \
                           "jse.executeScript(\"arguments[0].scrollIntoView(true);\", element);\n" \
                           "}\n" \
                           "} catch (NoSuchElementException e) {\n" \
                           "jse.executeScript(\"var ev = new MouseEvent('click', {'view': window," \
                           "'bubbles': true," \
                           "'cancelable': true," \
                           "'screenX': \"+x+\"," \
                           "'screenY': \"+y+\"" \
                           "});" \
                           "e=document.elementFromPoint(\"+x+\",\"+y+\");e.dispatchEvent(ev);\");\n" \
                           "flag=false;\n" \
                           "} catch (StaleElementReferenceException | ElementNotInteractableException ignored) {\n" \
                           "}\n" \
                           "}\n" \
                           "}"
    CHECK_TEXT = "this.checkText(By.xpath(\"{}\"));"
    CHECK_TEXT_METHOD = "private boolean checkText(By by) throws Exception {\n" \
                        "boolean found=false;\n" \
                        "boolean isTextPresent=false;\n" \
                        "for(int i=0; i < NUMBER_OF_ATTEMPTS; i++){\n" \
                        "try {\n" \
                        "driver.findElement(by);\n" \
                        "found = true;\n" \
                        "} catch (NoSuchElementException e){\n" \
                        "found = false;\n" \
                        "}\n" \
                        "if(found && !driver.findElement(by).getText().trim().equals(\"\")) {\n" \
                        "isTextPresent = true;\n" \
                        "} else {\n" \
                        "Thread.sleep(1000);\n" \
                        "}\n" \
                        "}\n" \
                        "return found&&isTextPresent;\n" \
                        "}"
    WAIT_FOR_RELOAD = "this.waitForReload();"
    WAIT_FOR_RELOAD_METHOD = "private void waitForReload() throws Exception {\n" \
                             "jse.executeScript(\"e=document.createElement(\\\"div\\\");" \
                             "e.setAttribute(\\\"id\\\", \\\"kebula-placeholder\\\");" \
                             "document.getElementsByTagName(\\\"body\\\")[0].appendChild(e);\");\n" \
                             "while (((ChromeDriver) driver).getSessionStorage().getItem(\"dtCookie\") == null ||\n" \
                             "jse.executeScript(\"return document.getElementById(\\\"kebula-placeholder\\\");\") != null){\n" \
                             "Thread.sleep(300);\n" \
                             "}\n" \
                             "}"

    @staticmethod
    def format_method(method_to_format):
        method = [line.strip() for line in method_to_format.split("\n")]
        method = '\n'.join(method)
        method = method.split("{")
        method_sign = method[0].strip()
        method = '{'.join(method[1:])
        method = method.rsplit("}", 1)[0]
        method = method.split('\n')
        method = method[:len(method) - 1]
        method = ["{}{}".format('\t', line) for line in method]
        method = '\n'.join(method)
        method = "{} {{{}\n}}\n".format(method_sign, method)
        return method


class JavaTestBuilder:
    def __init__(self, class_name, originals=None, lines=None, slots=None, intents=None, xpath=None, folder=None,
                 driver_path=None, parser_args=None):
        self.class_name = class_name
        self.intents = intents
        self.originals = originals
        self.lines = lines
        self.slots = slots
        self.xpath = xpath
        self.folder = folder
        self.test_statements = []
        self.test_class = None
        self.math_conditions = []
        self.web_conditions = []
        self.generated_conditions = []
        self.conditions_mode = None
        self.math_conditions_slot = []
        self.web_conditions_slot = []
        self._declared_vars = []
        self.url_extractor = URLExtract()
        self.test_class_pattern = None
        self.driver_path = driver_path
        self.parser_args = parser_args

    def generate_conditions(self):
        runner = SemanticParserV2(opt=get_args(self.conditions_mode)) if self.parser_args is None \
            else SemanticParserV2(opt=self.parser_args(self.conditions_mode))
        self.generated_conditions = runner.infer()

    def build(self, json_mapping=None, json_conditions=None,
              inference_file_path='../../data/graph2tree/infer/raw/test.txt'):
        if json_mapping is not None:
            with_cv = True
            self.build_test_method_from_json(json_mapping, json_conditions)
        else:
            with_cv = False
            self.extract_conditions(inference_file_path)
            self.generate_conditions()
            self.build_test_method()

        imports = '\n'.join(StatementsBuilder.IMPORTS)
        instance_vars = '\n'.join(StatementsBuilder.COMMON_INSTANCE_VARS)
        set_up_method = '\n'.join([StatementsBuilder.DRIVER_DECLARATION.format(self.driver_path)]
                                  + StatementsBuilder.SET_UP)
        set_up_annotations = '\n'.join(StatementsBuilder.SET_UP_ANNOTATIONS)
        set_up_method = '{}\npublic void setUp() {{\n{}\n}}'.format(set_up_annotations, set_up_method)
        set_up_method = StatementsBuilder.format_method(set_up_method)
        tear_down_method = '\n'.join(StatementsBuilder.TEAR_DOWN)
        tear_down_annotations = '\n'.join(StatementsBuilder.TEAR_DOWN_ANNOTATIONS)
        tear_down_method = '{}\npublic void tearDown() {{\n{}\n}}'.format(tear_down_annotations, tear_down_method)
        tear_down_method = StatementsBuilder.format_method(tear_down_method)
        test_method = '\n'.join(self.test_statements)
        test_annotations = '\n'.join(StatementsBuilder.TEST_ANNOTATIONS)
        test_method = '{}\npublic void doTest() throws Exception {{\n{}\n}}'.format(test_annotations, test_method)
        test_method = StatementsBuilder.format_method(test_method)
        if with_cv:
            self.test_class = '{}\n\n{}\n\npublic class {} {{\n{}\n\n{}\n\n{}\n\n{}\n{}\n{}\n}}'.format(
                StatementsBuilder.PACKAGE,
                imports,
                self.class_name,
                "{}\n{}".format(instance_vars, StatementsBuilder.TOOLBAR_HEIGHT_INSTANCE_VAR),
                set_up_method,
                tear_down_method,
                test_method,
                StatementsBuilder.format_method(StatementsBuilder.CLICK_WRAPPER_METHOD),
                StatementsBuilder.format_method(StatementsBuilder.WAIT_FOR_RELOAD_METHOD))
        else:
            self.test_class = '{}\n\n{}\n\npublic class {} {{\n{}\n\n{}\n\n{}\n\n{}\n}}'.format(
                StatementsBuilder.PACKAGE,
                imports,
                self.class_name,
                instance_vars,
                set_up_method,
                tear_down_method,
                test_method)

    def extract_conditions(self, inference_file_path):
        for line, slot, intent in zip(self.lines, self.slots, self.intents):
            if intent == 'test_math_condition':
                if 'the result' in ' '.join(line).lower():
                    line = ' '.join(line).replace('the result', 'R').split(' ')
                elif 'result' in ' '.join(line).lower():
                    line = ' '.join(line).replace('result', 'R').split(' ')
                self.math_conditions.append(line)
                self.math_conditions_slot.append(slot)
            elif intent == 'test_web_condition':
                self.web_conditions.append(line)
                self.web_conditions_slot.append(slot)

        if len(self.web_conditions) == 0:
            with open(inference_file_path, 'w') as mcf:
                math_conditions = ["{}\ttbd".format(' '.join(mc)) for mc in self.math_conditions]
                mcf.write('\n'.join(math_conditions))
                self.conditions_mode = 'math'
        elif len(self.math_conditions) == 0:
            with open(inference_file_path, 'w') as wcf:
                web_conditions = ["{}\ttbd".format(' '.join(wc)) for wc in self.web_conditions]
                wcf.write('\n'.join(web_conditions))
                self.conditions_mode = 'web'
        else:
            raise RuntimeError('Mode not supported')

    def build_test_method(self):
        condition_id = 0
        for original, line, slot, intent in zip(self.originals, self.lines, self.slots, self.intents):
            if intent == 'test_statement':
                self.test_statements.append(self._get_statement(line, slot, original=original))
            elif intent == 'test_math_condition':
                var_statement, condition_statement = self._get_math_statement(self.generated_conditions[condition_id])
                self.test_statements.append(var_statement)
                self.test_statements.append(condition_statement)
                condition_id += 1
            elif intent == 'test_web_condition':
                condition_statement = self._get_web_statement(self.generated_conditions[condition_id],
                                                              self.web_conditions[condition_id],
                                                              self.web_conditions_slot[condition_id])
                self.test_statements.append(condition_statement)
                condition_id += 1

    def build_test_method_from_json(self, mapping, conditions):
        action_element_number = 0
        conditions_id = 0
        # WORKAROUND
        is_enel = False
        for element in mapping:
            if element['action'] == 'navigate':
                self.test_statements.append(
                    StatementsBuilder.OPEN_LINK.format(parse.urljoin('https:', element['to'])))
                is_enel = True if 'www.enel' in element['to'] else False
            elif 'click' in element['action']:
                is_modal = str(element['is_modal']).lower()
                self.test_statements.append(StatementsBuilder.CLICK_WRAPPER.format(element['xpath'].replace('"', '\\"'),
                                                                                   is_modal,
                                                                                   element['x'],
                                                                                   element['y']))
                # WORKAROUND
                if action_element_number == 0 and is_enel:
                    self.test_statements.append(StatementsBuilder.WAIT_FOR_RELOAD)
                    action_element_number += 1
            elif 'fill' in element['action']:
                str_in = element['txt']
                find = StatementsBuilder.FIND_ELEMENT.format(element['xpath'].replace('"', '\\"'))
                send_keys = StatementsBuilder.SEND_KEYS.format(str_in)
                self.test_statements.append("{}.{};".format(find, send_keys))

            elif 'check' in element['action'] and len(conditions) > 0:
                c = conditions[str(conditions_id)]
                signature = c.split("(")[0].strip()
                negation = False
                if "_un" in signature:
                    negation = True

                xp = element['xpath'].replace('"', '\\"')
                if not negation:
                    signature = "{}(By.xpath(\"{}\")) != null".format("driver.findElement", xp)
                else:
                    signature = "{}(By.xpath(\"{}\")) == null".format("driver.findElement", xp)
                self.test_statements.append(G2TStatements.ASSERTION.format(signature))

                # self.test_statements.append(StatementsBuilder.CHECK_TEXT.format(element['xpath'].replace('"', '\\"')))
                conditions_id += 1

    def _get_math_statement(self, condition):
        # input binding
        var_name = 'R'
        statement = StatementsBuilder.STORE_VALUE.format(var_name, G2TStatements.RESULT_XPATH)
        if var_name not in self._declared_vars:
            self._declared_vars.append(var_name)
            statement = "{} {}".format(G2TStatements.VALUE_TYPE, statement)
        else:
            statement = "{}".format(statement)
        # conditions insertion
        condition = condition.replace("_", "{}.".format(G2TStatements.TRANSLATED_OP_CLASS)) \
            .replace(" ", "") \
            .replace("\n", "")
        return statement, G2TStatements.ASSERTION.format(condition)

    def _get_web_statement(self, condition, tokens, slots):
        string_sequence = []
        object_type = None
        # substitute object with xpath
        for i, entry in enumerate(zip(tokens, slots)):
            token, slot = entry
            if slot == 'O':
                continue
            elif slot.split(".")[0] == 'B-interface' and slot.split(".")[1] != 'object_label':
                object_type = slot
            elif slot.split(".")[1] == 'sequence':
                string_sequence.append(token)
        condition_formatted = condition
        for i, c_token in enumerate(condition.split('(')):
            if c_token.strip()[0] != '_':
                object_label = c_token.split(',')[0].split(')')[0].strip()
                find_statement = StatementsBuilder.FIND_ELEMENT \
                    .format(self.xpath[object_type.split('.')[-1]][object_label].replace('"', '\\"'))
                condition_formatted = condition_formatted.replace(object_label, find_statement)
        if len(string_sequence) > 0:
            condition_formatted = condition_formatted.replace('#sequence', ' '.join(string_sequence))
        condition_formatted = "{}.".format(G2TStatements.TRANSLATED_OP_CLASS).join(condition_formatted.split("_"))
        condition_formatted = condition_formatted.replace(' ', '')
        return G2TStatements.ASSERTION.format(condition_formatted)

    def _get_statement(self, line, slot, original=None):
        if 'B-interaction.visiting' in slot:
            url_idx = slot.index('B-input.url')
            translated_url = line[url_idx]
            original_url = self.url_extractor.find_urls(original)
            selected_url = original_url[0] if translated_url != original_url[0] else translated_url
            open_link_statement = StatementsBuilder.OPEN_LINK.format(parse.urljoin('http:', selected_url))
            ###### WORKAROUND ONLY FOR POC ######
            if 'enel.it' in selected_url:
                remove_banner_statements = '\n'.join(StatementsBuilder.REMOVE_COOKIE_BANNER)
                open_link_statement = "{}\n{}".format(open_link_statement, remove_banner_statements)
            ###### WORKAROUND ONLY FOR POC ######
            return open_link_statement
        else:
            # label oggetto ad es. first name etc
            object_label_idx = [i for i, s in enumerate(slot) if 'object_label' in s]
            object_label = '_'.join([line[i].strip() for i in object_label_idx]).lower()
            object_label = object_label.replace('\'', '_').replace('"', '')
            # tipo oggetto ad es. button
            object_type_idx = -1
            for i, s in enumerate(slot):
                if 'B-interface' in s and 'object_label' not in s:
                    object_type_idx = i
                    break
            object_type = slot[object_type_idx].split('.')[-1]
            object_xpath = self.xpath[object_type][object_label].replace('"', '\\"')
            # full find element by xpath statement
            object_statement = StatementsBuilder.FIND_ELEMENT.format(object_xpath)
            if 'B-interaction.pressure' in slot:
                return "{}.{};".format(object_statement, StatementsBuilder.CLICK)
            elif 'B-interaction.typing' in slot:
                # stringa da inserire
                object_input_idx = [i for i, s in enumerate(slot) if 'sequence' in s]
                object_input = ' '.join([line[i].strip() for i in object_input_idx]).replace('"', '')
                send_keys_statement = StatementsBuilder.SEND_KEYS.format(object_input)
                return "{}.{};".format(object_statement, send_keys_statement)
            elif 'B-interaction.selection' in slot:
                select_statement = StatementsBuilder.SELECT.format(object_statement)
                option_number_idx = [i for i, s in enumerate(slot) if 'option_number' in s]
                option_number = [line[i].strip() for i in option_number_idx]
                option = -1
                for on in option_number:
                    try:
                        option = int(on)
                    except ValueError:
                        continue
                select_by_index_statement = StatementsBuilder.SELECT_BY_INDEX.format(option)
                return "{}.{};".format(select_statement, select_by_index_statement)

    def persist(self):
        with open(os.path.join(self.folder, "{}.java".format(self.class_name)), 'w') as writer:
            writer.write(self.test_class)
